<?php

namespace Simcify\Controllers;

use Simcify\File;
use Simcify\Mail;
use Simcify\Auth;
use Simcify\Database;

class Home{

    /**
     * Get started view
     *
     * @return \Pecee\Http\Response
     */
    public function started(){

        $guest = 'hello';
        $signingLink = 'testlink';

        return view('getstarted',  compact("guest","signingLink"));
    }
}