<?php
namespace Simcify\Controllers;

use Simcify\File;
use Simcify\Mail;
use Simcify\Auth;
use Simcify\Database;
use Simcify\Str;
use Simcify\Signer;
class Signingrequests{

    /**
     * Get customers view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() { 
        $signingsrequests = Database::table("signingsrequests")->where("userID", Auth::user()->id)->get();
        return view('signingrequests', compact("signingsrequests"));
    }

    /**
     * Create customer account
     * 
     * @return Json
     */
    public function create() { 
        header('Content-type: application/json');
        if(isset($_POST['orderNumber'])){ 
            extract($_POST);
            $requestData = array(
                    "orderNumber" => $orderNumber,
                    "typeofSigning" => $typeofSigning,
                    "serviceType" => $serviceType,
                    "borrower1Name" => $borrower1Name,
                    "borrower1Email" => $borrower1Email,
                    "borrower1Cell" => $borrower1Cell,
                    "propertyAddress" => $propertyAddress,
                    "city" => $city,
                    "state" => $state,
                    "zip" => $zip,
                    "userID" => Auth::user()->id
                );
            $borrowerinfo = array();
            if(isset($borrowerName)){
                $i = 0;
                foreach ($borrowerName as $binfo) {
                    $borrowerinfo[] = $binfo.",".$borrower2Email[$i].",".$borrowercell[$i];
                    $i++;
                }
            }
            if(!empty($borrowerinfo)){
                $borrowerinfo = join(" | ",$borrowerinfo); 
                $requestData['extraBorrower'] = $borrowerinfo;
            }
            if($role == 'notary'){
                $getNotaryID = Database::table("notarydetails")->where("userID", Auth::user()->id)->first();
                $requestData['notaryID'] = $getNotaryID->notaryID;
            }
            if($role == 'client'){
                $getClientID = Database::table("clientdetails")->where("userID", Auth::user()->id)->first();
                $requestData['clientID'] = $getClientID->clientID;
            }
            $insert = Database::table("signingsrequests")->insert($requestData);
            if($insert){
              exit(json_encode(responder("success", "Signings Request Added", "Signings Request successfully added","reload()")));
            }else{
                exit(json_encode(responder("error", "Something Went Wrong", "Something Went Wrong","")));
            }
        }
    }

    /**
     * Delete Customer account
     * 
     * @return Json
     */
    public function delete() { 
        Database::table("signingsrequests")->where("orderID", input("orderID"))->delete();
        header('Content-type: application/json');
        exit(json_encode(responder("success", "Signing Request Deleted!", "Signing Request successfully deleted.","reload()")));
    }

    /**
     * Customer update view
     * 
     * @return Json
     */
    public function updateview() {
        $data = array(
                "signingsrequests" => Database::table("signingsrequests")->where("orderID", input("orderID"))->first()
            );

        return view('extras/updatesigningrequest', $data);
    }  

    public function offerview() { 
        $orderID = input("orderID");
        return view('signingrequest/offerpopup', compact("orderID"));
    }

    /**
     * Update user account
     * 
     * @return Json
     */

    public function details($id) { 
        $data = array(
                "signingsrequests" => Database::table("signingsrequests")->where("orderID", $id)->first(),
                "requestDoctumnets" => Database::table("files")->where("orderID", $id)->get()
            );
        return view('signingrequest/details', $data);
    }

    public function offers($id) { 
  
        $data = array(
                "signingoffers" => Database::table("sentofferstonotary")->where("orderID", $id)->leftJoin("users", "users.id","sentofferstonotary.notaryID")->get("`users.fname`","`users.lname`", "`sentofferstonotary.response`", "`sentofferstonotary.id`", "`sentofferstonotary.orderID`", "`sentofferstonotary.responseTime`", "`sentofferstonotary.status`", "`sentofferstonotary.dated`"),
                "signingsrequests" => Database::table("signingsrequests")->where("orderID", $id)->first(),
                "id" => $id,
            );
        return view('signingrequest/offers', $data);
    }

    public function assigned($id) { 
        $signingsRequests = Database::table("sentofferstonotary")->where("id", $id)->first();
        $AssignRequestToNotary = Database::table("signingsrequests")->where("orderID" , $signingsRequests->orderID)
        ->update(array(
            "notaryID" => $signingsRequests->notaryID,
        ));

        $AssignOfferStatus = Database::table("sentofferstonotary")->where("id" , $id)
        ->update(array(
            "status" => 3,
        ));

         redirect(url("signingrequestoffers/".$signingsRequests->orderID));
    }

    

    public function notification($id) { 

        $sentofferstonotary = Database::table("sentofferstonotary")->where("id", $id)->first();
        $signingsRequests = Database::table("signingsrequests")->where("orderID", $sentofferstonotary->orderID)->first();
        $notaryInfo = Database::table("users")->where("id", $signingsRequests->notaryID)->first();
        $files = Database::table("files")->where("orderID", $sentofferstonotary->orderID)->first();
        $notaryEmail = $notaryInfo->email;
        $notaryPhone = $notaryInfo->phone;
        $notaryName = $notaryInfo->fname." ".$notaryInfo->lname;
        $actualUrl = env("APP_URL")."/document/".$files->document_key;
                //============Send Emails to  borrowers===============// 
        $hashKey = $sentofferstonotary->orderID.Str::random(32);
        $createdUrl = env("APP_URL")."/accessurl/".$hashKey;
            $requestAccess = array(
                                "email" => $signingsRequests->borrower1Email,
                                "orderID" => $signingsRequests->orderID,
                                "hashKey" => $sentofferstonotary->orderID.Str::random(32),
                                "url" => $actualUrl,
                                "created_at" => date("Y-m-d h:i:s")
                            ); 
            $requestAccess = Database::table("signingrequestaccess")->insert($requestAccess);
                 //=============Mail Message ==========================//
            $message = '<table style="width:100%;line-height: 35px;">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" >
                                            Dear  '.$signingsRequests->borrower1Name.'! <br>
                                            You signing is scheduled with notary '.$notaryName.'  on '.date("d M, Y h:i:s",strtotime($RequestedAppointmentDate)).' please be online on the given schedule below are the signing details<br><br>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2" style="text-align: center;border: 1px solid;"><h3>Signing Request Details</h3></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <img src="http://maps.googleapis.com/maps/api/streetview?size=640x320&location='.urlencode($signingsRequests->propertyAddress).'&key=AIzaSyDliqDPl22E0Ifhhv7WAiwWWUFNrXnADzs" style="max-width: 100%;margin-bottom: 20px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">City</h3></td>
                                            <td>'.$signingsRequests->city.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">State</h3></td>
                                            <td>'.$signingsRequests->state.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Property Address</h3></td>
                                            <td>'.$signingsRequests->propertyAddress.'</td>
                                        </tr>
                                        <tr>
                                            <td ><h3 style="margin: 5px;">Order Number</h3></td>
                                            <td>'.$signingsRequests->orderNumber.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Notary Name</h3></td>
                                            <td>'.$notaryName.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Notary Email</h3></td>
                                            <td>'.$notaryEmail.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Notary Phone</h3></td>
                                            <td>'.$notaryPhone.'</td>
                                        </tr>
                                        '.$extraBorrowerContent.'
                                         <tr style="line-height: 150px;">
                                           <td colspan="2" style="text-align:center;">
                                               <a href="'.$createdUrl.'" target="_blank" style="font-family: Arial, Helvetica, sans-serif;box-sizing: border-box;">
                                                <button style="background-color: #007bff;border: 0;color: #ffffff;line-height: 22px;font-size: 18px;font-weight: bold;padding: 13px 24px;mso-line-height-rule: exactly;border-radius: 4px;text-align: left;text-align: center;font-family: Arial, Helvetica, sans-serif;box-sizing: border-box;">
                                                Click to go to session
                                                </button>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <table>';
                //=============Mail Message ==========================//
                    Mail::send(
                            $signingsRequests->borrower1Email,
                            "New Signing Requests Offer At ".env("APP_NAME")."!",
                            array(
                                "title" => "New Signing Requests Offer At ".env("APP_NAME")."!",
                                "subtitle" => "A new signings requests offer to you at ".env("APP_NAME").".",
                                "message" => $message
                            ),
                            "offer"
                        );
             
                 //============Send Emails to  borrowers===============//  


        $extraBorrower = $signingsRequests->extraBorrower;
        //============Send Emails to extra borrowers===============// 
        $extraBorrowerContent = '';
        if(!empty($extraBorrower)){
            $extraBorrower = explode(" | ",$extraBorrower);
            $brno = 1;
            foreach ($extraBorrower as $value) {
                if(!empty($value)){
                    $brno++;
                    $value = explode(",",$value);
        $extraBorrowerContent .= '<tr>
                                            <td><h3 style="margin: 5px;">Customer '.$brno.' Name</h3></td>
                                            <td>'.$value[0].'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Customer '.$brno.' Email</h3></td>
                                            <td>'.$value[1].'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Customer '.$brno.' Phone</h3></td>
                                            <td>'.$value[2].'</td>
                                        </tr>'; 
        $hashKey = $sentofferstonotary->orderID.Str::random(32);
        $createdUrl = env("APP_URL")."/accessurl/".$hashKey;
        $requestAccess = array(
                                "email" => $value[1],
                                "orderID" => $signingsRequests->orderID,
                                "hashKey" => $sentofferstonotary->orderID.Str::random(32),
                                "url" => $actualUrl
                            ); 
        $requestAccess = Database::table("signingrequestaccess")->insert($requestAccess);        
                                  
      //=============Mail Message With offer Details==========================//
            $message = '<table style="width:100%;line-height: 35px;">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" >
                                            Dear  '.$value[0].'! <br>
                                            You signing is scheduled with notary '.$notaryName.'  on '.date("d M, Y h:i:s",strtotime($RequestedAppointmentDate)).' please be online on the given schedule below are the signing details<br><br>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2" style="text-align: center;border: 1px solid;"><h3>Signing Request Details</h3></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <img src="http://maps.googleapis.com/maps/api/streetview?size=640x320&location='.urlencode($signingsRequests->propertyAddress).'&key=AIzaSyDliqDPl22E0Ifhhv7WAiwWWUFNrXnADzs" style="max-width: 100%;margin-bottom: 20px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">City</h3></td>
                                            <td>'.$signingsRequests->city.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">State</h3></td>
                                            <td>'.$signingsRequests->state.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Property Address</h3></td>
                                            <td>'.$signingsRequests->propertyAddress.'</td>
                                        </tr>
                                        <tr>
                                            <td ><h3 style="margin: 5px;">Order Number</h3></td>
                                            <td>'.$signingsRequests->orderNumber.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Notary Name</h3></td>
                                            <td>'.$notaryName.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Notary Email</h3></td>
                                            <td>'.$notaryEmail.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Notary Phone</h3></td>
                                            <td>'.$notaryPhone.'</td>
                                        </tr>
                                         <tr style="line-height: 150px;">
                                            <td colspan="2" style="text-align:center;">
                                               <a href="'.$actualUrl.'" target="_blank" style="font-family: Arial, Helvetica, sans-serif;box-sizing: border-box;">
                                                <button style="background-color: #007bff;border: 0;color: #ffffff;line-height: 22px;font-size: 18px;font-weight: bold;padding: 13px 24px;mso-line-height-rule: exactly;border-radius: 4px;text-align: left;text-align: center;font-family: Arial, Helvetica, sans-serif;box-sizing: border-box;">
                                                Click to go to session
                                                </button>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <table>';
            //=============Mail Message ==========================//
                    Mail::send(
                            $value[1],
                            "New Signing Requests Offer At ".env("APP_NAME")."!",
                            array(
                                "title" => "New Signing Requests Offer At ".env("APP_NAME")."!",
                                "subtitle" => "A new signings requests offer to you at ".env("APP_NAME").".",
                                "message" => $message
                            ),
                            "offer"
                        );
              
                }
                
            }
        }
        //============Send Emails to extra borrowers===============//  

                 //============Send Emails to  Notary===============// 

                 //=============Mail Message ==========================//
        $requestAccess = array(
                                "email" => $notaryEmail,
                                "orderID" => $signingsRequests->orderID,
                                "hashKey" => $sentofferstonotary->orderID.Str::random(32),
                                "url" => $actualUrl
                            ); 
        $requestAccess = Database::table("signingrequestaccess")->insert($requestAccess);
            $message = '<table style="width:100%;line-height: 35px;">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" >
                                            Dear  '.$notaryName.'! <br>
                                            You are scheduled for the signing on '.date("d M, Y h:i:s",strtotime($RequestedAppointmentDate)).' please be online on the given schedule below are the signing details<br><br>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2" style="text-align: center;border: 1px solid;"><h3>Signing Request Details</h3></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <img src="http://maps.googleapis.com/maps/api/streetview?size=640x320&location='.urlencode($signingsRequests->propertyAddress).'&key=AIzaSyDliqDPl22E0Ifhhv7WAiwWWUFNrXnADzs" style="max-width: 100%;margin-bottom: 20px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">City</h3></td>
                                            <td>'.$signingsRequests->city.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">State</h3></td>
                                            <td>'.$signingsRequests->state.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Property Address</h3></td>
                                            <td>'.$signingsRequests->propertyAddress.'</td>
                                        </tr>
                                        <tr>
                                            <td ><h3 style="margin: 5px;">Order Number</h3></td>
                                            <td>'.$signingsRequests->orderNumber.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Customer Name</h3></td>
                                            <td>'.$signingsRequests->borrower1Name.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Customer Email</h3></td>
                                            <td>'.$signingsRequests->borrower1Email.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="margin: 5px;">Customer Phone</h3></td>
                                            <td>'.$signingsRequests->borrower1Cell.'</td>
                                        </tr>
                                        '.$extraBorrowerContent.'
                                         <tr style="line-height: 150px;">
                                            <td colspan="2" style="text-align:center;">
                                               <a href="'.$actualUrl.'" target="_blank" style="font-family: Arial, Helvetica, sans-serif;box-sizing: border-box;">
                                                <button style="background-color: #007bff;border: 0;color: #ffffff;line-height: 22px;font-size: 18px;font-weight: bold;padding: 13px 24px;mso-line-height-rule: exactly;border-radius: 4px;text-align: left;text-align: center;font-family: Arial, Helvetica, sans-serif;box-sizing: border-box;">
                                                Click to go to session
                                                </button>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <table>'; 
                //=============Mail Message ==========================//
                    Mail::send(
                            $notaryEmail,
                            "New Signing Requests Offer At ".env("APP_NAME")."!",
                            array(
                                "title" => "New Signing Requests Offer At ".env("APP_NAME")."!",
                                "subtitle" => "A new signings requests offer to you at ".env("APP_NAME").".",
                                "message" => $message
                            ),
                            "offer"
                        );
             
                 //============Send Emails to  Notary===============//  

        redirect(url("signingrequestoffers/".$signingsRequests->orderID));


    }

    public function update() {
        extract($_POST);
        $borrowerinfo = array();
            if(isset($borrowerName)){
                $i = 0;
                foreach ($borrowerName as $binfo) {
                    $borrowerinfo[] = $binfo.",".$borrower2Email[$i].",".$borrowercell[$i];
                    $i++;
                }
            }
            if(!empty($borrowerinfo)){
                $extraBorrower = join(" | ",$borrowerinfo); 
            }else{
                $extraBorrower = '';
            }
        Database::table("signingsrequests")->where("orderID" , $orderID)
        ->update(array(
            "orderNumber" => $orderNumber,
            "typeofSigning" => $typeofSigning,
            "serviceType" => $serviceType,
            "borrower1Name" => $borrower1Name,
            "borrower1Email" => $borrower1Email,
            "borrower1Cell" => $borrower1Cell,
            "extraBorrower" => $extraBorrower,
            "propertyAddress" => $propertyAddress,
            "city" => $city,
            "state" => $state,
            "zip" => $zip,
        ));
        header('Content-type: application/json');
        exit(json_encode(responder("success", "Alright", "Signings Requests successfully updated","reload()")));
    }

    public function offerdetails() { 
        extract($_POST);
        if($status == 1){
            $response = 'Accepted';
        }else{
            $response = 'Rejected';
        }
        $update = Database::table("sentofferstonotary")->where("id" , $rowID)
        ->update(array(
            "status" => $status,
            "response" => $response,
            "responseTime" => date("Y-m-d h:i:s"),
        ));

        
        if($update){
            header('Content-type: application/json');
            exit(json_encode(responder("success", "Alright", "Signings Requests ".$response." successfully","reload()")));
        }else{
            exit(json_encode(responder("error", "Something Went Wrong", "Something Went Wrong","")));
        }
        
    }

    public function uploadfile() { 
        header('Content-type: application/json');
        $user = Auth::user();
        $data = array(
                        "company" => $user->company,
                        "uploaded_by" => $user->id,
                        "name" => input("name"),
                        "folder" => 1,
                        "file" => $_FILES['file'],
                        "is_template" => "Yes",
                        "orderID" => $_POST['orderID'],
                        "source" => "form",
                        "document_key" => Str::random(32),
                        "activity" => 'Template uploaded by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span>.'
                    );
        $upload = Signer::upload($data);
        if ($upload['status'] == "success") {
            exit(json_encode(responder("success", "Your Document Uploaded Successfully! Please Mark Up Your Document", "","reload()", "true")));
        }else{
            exit(json_encode(responder("error", "Oops!", $upload['message'])));
        }
    }

    public function assigntonotary() { 

        if(isset($_POST['signing_request_order_id'])){ //echo "<pre>"; print_r($_POST); die;
            extract($_POST);
            $signingsrequests =  Database::table("signingsrequests")->where("orderID", $signing_request_order_id)->first();
            //=============Mail Message With offer Details==========================//
            $message = '<table style="width:100%;line-height: 35px;">
                                    <tbody>
                                    <tr>
                                            <td colspan="2" style="text-align: center;border: 1px solid;"><h3>Signing Request Details</h3></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <img src="http://maps.googleapis.com/maps/api/streetview?size=640x320&location='.urlencode($signingsrequests->propertyAddress).'&key=AIzaSyDliqDPl22E0Ifhhv7WAiwWWUFNrXnADzs" style="max-width: 100%;margin-bottom: 20px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td ><h3>Order Number</h3></td>
                                            <td>'.$signingsrequests->orderNumber.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Customer Name</h3></td>
                                            <td>'.$signingsrequests->borrower1Name.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Customer Email</h3></td>
                                            <td>'.$signingsrequests->borrower1Email.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Customer Phone</h3></td>
                                            <td>'.$signingsrequests->borrower1Cell.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3>City</h3></td>
                                            <td>'.$signingsrequests->city.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3>State</h3></td>
                                            <td>'.$signingsrequests->state.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Property Address</h3></td>
                                            <td>'.$signingsrequests->propertyAddress.'</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Requested Appointment Date Time</h3></td>
                                            <td>'.date("Y-m-d h:i:s",strtotime($RequestedAppointmentDate)).'</td>
                                        </tr>
                                    </tbody>
                                    <table>';
            //=============Mail Message ==========================//
            
            //==============Get All Available Notaries==================//  
            $allnotary =  Database::table("users")->where("role",'notary')->get();  
            //==============Get All Available Notaries==================//                     
            if(!empty($allnotary)){
                foreach ($allnotary as $nid) {

                    $checkAvailability = Database::table("signingsrequests")->where("notaryID", $nid->id)->where("status",1)->get();
                    if(!empty($checkAvailability)){
                        continue;
                    }
                    //============Send Emails to notary===============//
                    Mail::send(
                            $nid->email,
                            "New Signing Requests Offer At ".env("APP_NAME")."!",
                            array(
                                "title" => "New Signing Requests Offer At ".env("APP_NAME")."!",
                                "subtitle" => "A new signings requests offer to you at ".env("APP_NAME").".",
                                "message" => $message
                            ),
                            "offer"
                        );

            $requestData = array(
                                "notaryID" => $nid->id,
                                "orderID" => $signing_request_order_id,
                                "hashKey" => Str::random(32),
                                "requestedAppointmentDateTime"=> date("Y-m-d h:i:s",strtotime($RequestedAppointmentDate))
                            ); 
            $insertOffer = Database::table("sentofferstonotary")->insert($requestData);
            $sent_id = Database::table("sentofferstonotary")->insertId();
            if(!empty($sent_id)){
                $requestData = array(
                                "user" => $nid->id,
                                "message" => 'You have 1 New Signing Requests Offer',
                                "sentOfferID" => $sent_id,
                                "type" => 'accept',
                            ); 
                $insertNotification = Database::table("notifications")->insert($requestData);

            }
            
                    //============Send Emails to notary===============//
                }
                if($insertNotification){
                    header('Content-type: application/json');
                    exit(json_encode(responder("success", "Alright", "Signing Request offer send successfully","reload()")));
                }else{
                     exit(json_encode(responder("error", "Something Went Wrong", "Something Went Wrong","")));
                }
            }
          
        
        
        }
    }

}
