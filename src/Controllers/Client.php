<?php
namespace Simcify\Controllers;

use Simcify\Auth as Authenticate;
use Simcify\Database;

class Client{

    /**
     * Get Auth view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() { 
        $guest = $signingLink = false;
        if (isset($_COOKIE['guest'])) {
            $guest = true;
            $guestData = unserialize($_COOKIE['guest']);
            $signingLink = url("Guest@open").$guestData[0]."?signingKey=".$guestData[1];
        }
        if (!isset($_GET['secure'])) {
            redirect(url("Auth@get")."?secure=true");
        }
        $states = Database::table("state")->get();

        // return view('login',array('states'=>$states), compact("guest","signingLink"));
        $data = array(
          "states" => $states
          );
        //view('companies', $data);

        return view('clientlogin',$data,compact("guest","signingLink"));
    }

    /**
     * Sign In a user
     * 
     * @return Json
     */
    public function signin() { 
        $signIn = Authenticate::login(
		    input('email'), 
		    input('password'), 
		    array(
		        "rememberme" => true,
		        "redirect" => url(""),
		        "status" => "Active"
		    )
		);
        header('Content-type: application/json');
		exit(json_encode($signIn));
    }

    /**
     * Forgot password - send reset password email
     * 
     * @return Json
     */
    public function forgot() {
        $forgot = Authenticate::forgot(
		    input('email'), 
		    env('APP_URL')."/reset/[token]"
		);
        header('Content-type: application/json');
		exit(json_encode($forgot));
    }

    /**
     * Get reset password view
     * 
     * @return \Pecee\Http\Response
     */
    public function getreset($token) {
        return view('reset', array("token" => $token));
    }

    /**
     * Reset password
     * 
     * @return Json
     */
    public function reset() {
        $reset = Authenticate::reset(
		    input('token'), 
		    input('password')
		);

        header('Content-type: application/json');
		exit(json_encode($reset));
    }

    /**
     * Create an account
     * 
     * @return Json
     */

    public function signup() { 

        extract($_POST);
        $timezone = Database::table("state")->where("stateID", $state)->first();
        $signup = Authenticate::signup(
            array(
                "fname" => $fname,
                "lname" => $lname,
                "email" => $email,
                "phone" => $phone,
                "address" => $address,
                "city" => $city,
                "state" => $state,
                "zip" => $zip,
                "timezone" => $timezone->timezone_value,
                "role" => "client",
                "password" => Authenticate::password(input('password'))
            ), 
            array(
                "authenticate" => true,
                "redirect" => url(""),
                "uniqueEmail" => input('email'),
                "type" => 'client',
            )
        );

        $usersid = $signup['newUserId'];

        if(!empty($usersid)){
            $clientData = array(
                    "userID" => $usersid,
                    "company" => $company
                );
            $insert = Database::table("clientdetails")->insert($clientData);
            //==================Client Services====================//

            $clientID = Database::table("clientdetails")->insertId();
            if(!empty($clientID)){
                $defaultServices = Database::table("services")->get();
                if(!empty($defaultServices)){
                    foreach ($defaultServices as $ds) {
                        //================Add Default Services into clientservices==============//
                            $clientServicesData = array(
                                    "service_id" => $ds->id,
                                    "chargeToCustomer" => $ds->chargeToCustomer,
                                    "payToNotary" => $ds->payToNotary,
                                    "clientID" => $clientID
                                );
                            $insert = Database::table("clientservices")->insert($clientServicesData);

                        //================Add Default Services into clientservices==============//
                    }
                }
                header('Content-type: application/json');
                exit(json_encode($signup));
            }
            
            //==================Client Services====================//
            
            
        }
        
    }
    /**
     * Sign Out a logged in user
     *
     */
    public function signout() {
        Authenticate::deauthenticate();
        redirect(url("Auth@get"));

    }

}
