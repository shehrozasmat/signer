<?php
namespace Simcify\Controllers;

use Simcify\Auth as Authenticate;
use Simcify\Database;

class Auth{

    /**
     * Get Auth view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() { 
        $guest = $signingLink = false;
        if (isset($_COOKIE['guest'])) {
            $guest = true;
            $guestData = unserialize($_COOKIE['guest']);
            $signingLink = url("Guest@open").$guestData[0]."?signingKey=".$guestData[1];
        }
        if (!isset($_GET['secure'])) {
            redirect(url("Auth@get")."?secure=true");
        }
        $states = Database::table("state")->get();

        // return view('login',array('states'=>$states), compact("guest","signingLink"));
        $data = array(
          "states" => $states
          );
        //view('companies', $data);

        return view('login',$data,compact("guest","signingLink"));
    }

    /**
     * Sign In a user
     * 
     * @return Json
     */
    public function signin() { 
        $signIn = Authenticate::login(
		    input('email'), 
		    input('password'), 
		    array(
		        "rememberme" => true,
		        "redirect" => url(""),
		        "status" => "Active"
		    )
		);
        header('Content-type: application/json');
		exit(json_encode($signIn));
    }

    /**
     * Forgot password - send reset password email
     * 
     * @return Json
     */
    public function forgot() {
        $forgot = Authenticate::forgot(
		    input('email'), 
		    env('APP_URL')."/reset/[token]"
		);
        header('Content-type: application/json');
		exit(json_encode($forgot));
    }

    /**
     * Get reset password view
     * 
     * @return \Pecee\Http\Response
     */
    public function getreset($token) {
        return view('reset', array("token" => $token));
    }

    /**
     * Reset password
     * 
     * @return Json
     */
    public function reset() {
        $reset = Authenticate::reset(
		    input('token'), 
		    input('password')
		);

        header('Content-type: application/json');
		exit(json_encode($reset));
    }

    /**
     * Create an account
     * 
     * @return Json
     */
    public function signup() {

    	if (!empty(input('business'))) {
    		$companyData = array(
    				"name" => input('company'),
    				"email" => input('email')
    			);
	        $insert = Database::table("companies")->insert($companyData);
	        $companyId = Database::table("companies")->insertId();
    	}else{
    		$companyId = 0;
    	}


        $signup = Authenticate::signup(
		    array(
		        "fname" => input('fname'),
		        "lname" => input('lname'),
		        "email" => input('email'),
		        "role" => "admin",
		        "company" => $companyId,
		        "password" => Authenticate::password(input('password'))
		    ), 
		    array(
		        "authenticate" => true,
		        "redirect" => url(""),
		        "uniqueEmail" => input('email')
		    )
		);

        header('Content-type: application/json');
		exit(json_encode($signup));
    }

    public function signupnotary() { 

        extract($_POST);
        $timezone = Database::table("state")->where("stateID", $state)->first();
        $signup = Authenticate::signup(
            array(
                "fname" => $fname,
                "lname" => $lname,
                "email" => $email,
                "phone" => $phone,
                "address" => $address,
                "city" => $city,
                "state" => $state,
                "zip" => $zip,
                "timezone" => $timezone->timezone_value,
                "role" => "notary",
                "password" => Authenticate::password(input('password'))
            ), 
            array(
                "authenticate" => true,
                "redirect" => url(""),
                "uniqueEmail" => input('email')
            )
        );

        $usersid = $signup['newUserId'];

        if(!empty($usersid)){
            $notaryData = array(
                    "userID" => $usersid,
                    "notaryType" => $notaryType
                );
            $insert = Database::table("notarydetails")->insert($notaryData);

            header('Content-type: application/json');
            exit(json_encode($signup));
        }
        
    }

    public function signupclient() { 

        extract($_POST);
        $timezone = Database::table("state")->where("stateID", $state)->first();
        $signup = Authenticate::signup(
            array(
                "fname" => $fname,
                "lname" => $lname,
                "email" => $email,
                "phone" => $phone,
                "address" => $address,
                "city" => $city,
                "state" => $state,
                "zip" => $zip,
                "timezone" => $timezone->timezone_value,
                "role" => "notary",
                "password" => Authenticate::password(input('password'))
            ), 
            array(
                "authenticate" => true,
                "redirect" => url(""),
                "uniqueEmail" => input('email')
            )
        );

        $usersid = $signup['newUserId'];

        if(!empty($usersid)){
            $notaryData = array(
                    "userID" => $usersid,
                    "company" => $company
                );
            $insert = Database::table("clientdetails")->insert($notaryData);
            
            header('Content-type: application/json');
            exit(json_encode($signup));
        }
        
    }
    /**
     * Sign Out a logged in user
     *
     */
    public function signout() {
        Authenticate::deauthenticate();
        redirect(url("Auth@get"));

    }

}
