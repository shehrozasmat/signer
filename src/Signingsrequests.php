<?php
namespace Simcify;

use Simcify\Str;

class Signingsrequests {


    /**
     * Login a user
     * 
     * @param string $username
     * @param password $password
     * @param string $options
     * @return mixed
     */
    public static function insert($requestData=array()) { 
        $insert = Database::table("signingsrequests")->insert($requestData);
        $requestId = Database::table("signingsrequests")->insertId();
        if(!empty($requestId)){
          exit(json_encode(responder("success", "Signings Request Added", "Signings Request successfully added","reload()")));
        }else{
            exit(json_encode(responder("error", "Signings Request Added", "Signings Request successfully added","reload()")));
        }
    }


}