SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_modifications`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientDetails`
--

CREATE TABLE `clientdetails` (
  `clientID` int(11) NOT NULL,
  `company` varchar(50) NOT NULL,
  `businessPhone` varchar(20) DEFAULT NULL,
  `afterHoursPhone` varchar(15) DEFAULT NULL,
  `birthDay` int(11) DEFAULT NULL,
  `birthMonth` int(11) DEFAULT NULL,
  `defaultPayment` varchar(100) DEFAULT NULL,
  `printFee` varchar(100) DEFAULT NULL,
  `noSignFee` varchar(100) DEFAULT NULL,
  `redrawFee` varchar(100) DEFAULT NULL,
  `cancelledFee` varchar(100) DEFAULT NULL,
  `addtionalTerms` varchar(100) DEFAULT NULL,
  `shippingMethod` varchar(100) DEFAULT NULL,
  `isShippingAccountNumber` tinyint(1) DEFAULT NULL,
  `shippingAccountNumber` varchar(100) DEFAULT NULL,
  `shippingAccountLabel` varchar(100) DEFAULT NULL,
  `orderNotificationEmails` tinyint(1) DEFAULT NULL,
  `serviceType` int(11) DEFAULT NULL,
  `customTierFee` varchar(10) DEFAULT NULL,
  `emailDocsPrice` varchar(50) DEFAULT NULL,
  `sellerSigningsPrice` varchar(50) DEFAULT NULL,
  `firstAndSecondPrice` varchar(50) DEFAULT NULL,
  `invoiceTo` varchar(10) DEFAULT NULL,
  `firstVisitonDashboard` int(11) DEFAULT NULL,
  `firstVisitonSearch` int(11) DEFAULT NULL,
  `weeklyInvoice` tinyint(1) DEFAULT NULL,
  `emailPlan` varchar(15) DEFAULT NULL,
  `noAttachAssignOrder` tinyint(1) DEFAULT NULL,
  `blockProfile` tinyint(1) DEFAULT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `clientUsers`
-- (See below for the actual view)
--
CREATE TABLE `clientUsers` (
    `clientID` int(11)
    ,`company` varchar(50)
    ,`businessPhone` varchar(20)
    ,`afterHoursPhone` varchar(15)
    ,`birthDay` int(11)
    ,`birthMonth` int(11)
    ,`defaultPayment` varchar(100)
    ,`printFee` varchar(100)
    ,`noSignFee` varchar(100)
    ,`redrawFee` varchar(100)
    ,`cancelledFee` varchar(100)
    ,`addtionalTerms` varchar(100)
    ,`shippingMethod` varchar(100)
    ,`isShippingAccountNumber` tinyint(1)
    ,`shippingAccountNumber` varchar(100)
    ,`shippingAccountLabel` varchar(100)
    ,`orderNotificationEmails` tinyint(1)
    ,`serviceType` int(11)
    ,`customTierFee` varchar(10)
    ,`emailDocsPrice` varchar(50)
    ,`sellerSigningsPrice` varchar(50)
    ,`firstAndSecondPrice` varchar(50)
    ,`invoiceTo` varchar(10)
    ,`firstvisitondashboard` int(11)
    ,`firstvisitonsearch` int(11)
    ,`weeklyInvoice` tinyint(1)
    ,`emailPlan` varchar(15)
    ,`noAttachAssignOrder` tinyint(1)
    ,`blockProfile` tinyint(1)
    ,`userID` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `notaryDetails`
--

CREATE TABLE `notarydetails` (
  `notaryID` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `phone2` varchar(15) DEFAULT NULL,
  `phone3` varchar(15) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `introduction` text DEFAULT NULL,
  `canReceiveDocViaEmail` tinyint(1) DEFAULT NULL,
  `haveLaserPrinter` tinyint(1) DEFAULT NULL,
  `canPrintLegalSize` tinyint(1) DEFAULT NULL,
  `haveADualTrayPrinter` tinyint(1) DEFAULT NULL,
  `foreignLanguages` tinyint(1) DEFAULT NULL,
  `notaryExperience` varchar(100) DEFAULT NULL,
  `haveExperienceInSigning` tinyint(1) DEFAULT NULL,
  `canContactReference` tinyint(1) DEFAULT NULL,
  `signingsPerformed` varchar(100) DEFAULT NULL,
  `loanSigningMethod` varchar(100) DEFAULT NULL,
  `notaryComissionNumber` varchar(50) DEFAULT NULL,
  `notaryComissionExpiration` varchar(50) DEFAULT NULL,
  `isBounder` tinyint(1) DEFAULT NULL,
  `boundNumber` varchar(50) DEFAULT NULL,
  `boundAmount` varchar(50) DEFAULT NULL,
  `boundExpiration` varchar(50) DEFAULT NULL,
  `isInsurance` tinyint(1) DEFAULT NULL,
  `insuranceNumber` varchar(50) DEFAULT NULL,
  `insuranceAmount` varchar(50) DEFAULT NULL,
  `insuranceExpiration` varchar(50) DEFAULT NULL,
  `fideltyApproved` tinyint(1) DEFAULT NULL,
  `fideltyApprovalType` varchar(100) DEFAULT NULL,
  `additionalInformation` varchar(100) DEFAULT NULL,
  `prolinkSignings` int(11) DEFAULT NULL,
  `textReminder` tinyint(1) DEFAULT NULL,
  `preferredContactMethodSMS` tinyint(1) DEFAULT NULL,
  `preferredContactMethodCall` tinyint(1) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `notaryScore` int(11) DEFAULT NULL,
  `omit` tinyint(1) DEFAULT NULL,
  `stripeConnectedAccount` varchar(100) DEFAULT NULL,
  `stripeAccountVerified` tinyint(1) DEFAULT NULL,
  `signature` varchar(100) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `adminFav` int(11) DEFAULT NULL,
  `eliteUser` int(11) DEFAULT NULL,
  `facebookID` varchar(250) DEFAULT NULL,
  `instagramID` varchar(250) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `car` varchar(255) DEFAULT NULL,
  `taxID` varchar(255) DEFAULT NULL,
  `ssnOrEin` varchar(255) DEFAULT NULL,
  `photoApprove` tinyint(1) DEFAULT NULL,
  `visitCount` int(11) DEFAULT NULL,
  `lastNotaryLogin` datetime DEFAULT NULL,
  `smsDisabled` tinyint(4) DEFAULT NULL,
  `hideNotary` tinyint(4) DEFAULT NULL,
  `oneTimeAccess` tinyint(1) DEFAULT NULL,
  `notaryType` varchar(100) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `notaryUsers`
-- (See below for the actual view)
--
CREATE TABLE `notaryUsers` (
                               `notaryID` int(11)
    ,`phone` varchar(20)
    ,`phone2` varchar(15)
    ,`phone3` varchar(15)
    ,`video` varchar(200)
    ,`introduction` text
    ,`canReceiveDocViaEmail` tinyint(1)
    ,`haveLaserPrinter` tinyint(1)
    ,`canPrintLegalSize` tinyint(1)
    ,`haveADualTrayPrinter` tinyint(1)
    ,`foreignLanguages` tinyint(1)
    ,`notaryExperience` varchar(100)
    ,`haveExperienceInSigning` tinyint(1)
    ,`canContactReference` tinyint(1)
    ,`signingsPerformed` varchar(100)
    ,`loanSigningMethod` varchar(100)
    ,`notaryComissionNumber` varchar(50)
    ,`notaryComissionExpiration` varchar(50)
    ,`isBounder` tinyint(1)
    ,`boundNumber` varchar(50)
    ,`boundAmount` varchar(50)
    ,`boundExpiration` varchar(50)
    ,`isInsurance` tinyint(1)
    ,`insuranceNumber` varchar(50)
    ,`insuranceAmount` varchar(50)
    ,`insuranceExpiration` varchar(50)
    ,`fideltyApproved` tinyint(1)
    ,`fideltyApprovalType` varchar(100)
    ,`additionalInformation` varchar(100)
    ,`prolinkSignings` int(11)
    ,`textReminder` tinyint(1)
    ,`preferredContactMethodSMS` tinyint(1)
    ,`preferredContactMethodCall` tinyint(1)
    ,`color` varchar(100)
    ,`notaryScore` int(11)
    ,`omit` tinyint(1)
    ,`stripeConnectedAccount` varchar(100)
    ,`stripeAccountVerified` tinyint(1)
    ,`signature` varchar(100)
    ,`slug` varchar(150)
    ,`adminFav` int(11)
    ,`eliteUser` int(11)
    ,`facebookID` varchar(250)
    ,`instagramID` varchar(250)
    ,`website` varchar(255)
    ,`car` varchar(255)
    ,`taxID` varchar(255)
    ,`ssnOrEin` varchar(255)
    ,`photoApprove` tinyint(1)
    ,`visitCount` int(11)
    ,`lastnotarylogin` datetime
    ,`smsDisabled` tinyint(4)
    ,`hideNotary` tinyint(4)
    ,`oneTimeAccess` tinyint(1)
    ,`userID` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `userID` int(11) NOT NULL,
                         `firstName` varchar(50) NOT NULL,
                         `lastName` varchar(50) NOT NULL,
                         `email` varchar(50) NOT NULL,
                         `password` varchar(50) NOT NULL,
                         `photo` varchar(200) NOT NULL,
                         `streetAddress` varchar(200) NOT NULL,
                         `city` varchar(30) NOT NULL,
                         `state` varchar(30) NOT NULL,
                         `zip` varchar(30) NOT NULL,
                         `lat` varchar(30) NOT NULL,
                         `lng` varchar(30) NOT NULL,
                         `companiesID` int(11) NOT NULL,
                         `testProfile` tinyint(1) NOT NULL,
                         `status` tinyint(1) NOT NULL,
                         `dated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `clientUsers`
--
DROP TABLE IF EXISTS `clientUsers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `clientUsers`  AS  select `c`.`clientID` AS `clientID`,`c`.`company` AS `company`,`c`.`businessPhone` AS `businessPhone`,`c`.`afterHoursPhone` AS `afterHoursPhone`,`c`.`birthDay` AS `birthDay`,`c`.`birthMonth` AS `birthMonth`,`c`.`defaultPayment` AS `defaultPayment`,`c`.`printFee` AS `printFee`,`c`.`noSignFee` AS `noSignFee`,`c`.`redrawFee` AS `redrawFee`,`c`.`cancelledFee` AS `cancelledFee`,`c`.`addtionalTerms` AS `addtionalTerms`,`c`.`shippingMethod` AS `shippingMethod`,`c`.`isShippingAccountNumber` AS `isShippingAccountNumber`,`c`.`shippingAccountNumber` AS `shippingAccountNumber`,`c`.`shippingAccountLabel` AS `shippingAccountLabel`,`c`.`orderNotificationEmails` AS `orderNotificationEmails`,`c`.`serviceType` AS `serviceType`,`c`.`customTierFee` AS `customTierFee`,`c`.`emailDocsPrice` AS `emailDocsPrice`,`c`.`sellerSigningsPrice` AS `sellerSigningsPrice`,`c`.`firstAndSecondPrice` AS `firstAndSecondPrice`,`c`.`invoiceTo` AS `invoiceTo`,`c`.`firstVisitonDashboard` AS `firstvisitondashboard`,`c`.`firstVisitonSearch` AS `firstvisitonsearch`,`c`.`weeklyInvoice` AS `weeklyInvoice`,`c`.`emailPlan` AS `emailPlan`,`c`.`noAttachAssignOrder` AS `noAttachAssignOrder`,`c`.`blockProfile` AS `blockProfile`,`c`.`userID` AS `userID` from (`clientDetails` `c` join `users` `u`) where (`c`.`userID` = `u`.`userID`) ;

-- --------------------------------------------------------

--
-- Structure for view `notaryUsers`
--
DROP TABLE IF EXISTS `notaryUsers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `notaryUsers`  AS  select `n`.`notaryID` AS `notaryID`,`n`.`phone` AS `phone`,`n`.`phone2` AS `phone2`,`n`.`phone3` AS `phone3`,`n`.`video` AS `video`,`n`.`introduction` AS `introduction`,`n`.`canReceiveDocViaEmail` AS `canReceiveDocViaEmail`,`n`.`haveLaserPrinter` AS `haveLaserPrinter`,`n`.`canPrintLegalSize` AS `canPrintLegalSize`,`n`.`haveADualTrayPrinter` AS `haveADualTrayPrinter`,`n`.`foreignLanguages` AS `foreignLanguages`,`n`.`notaryExperience` AS `notaryExperience`,`n`.`haveExperienceInSigning` AS `haveExperienceInSigning`,`n`.`canContactReference` AS `canContactReference`,`n`.`signingsPerformed` AS `signingsPerformed`,`n`.`loanSigningMethod` AS `loanSigningMethod`,`n`.`notaryComissionNumber` AS `notaryComissionNumber`,`n`.`notaryComissionExpiration` AS `notaryComissionExpiration`,`n`.`isBounder` AS `isBounder`,`n`.`boundNumber` AS `boundNumber`,`n`.`boundAmount` AS `boundAmount`,`n`.`boundExpiration` AS `boundExpiration`,`n`.`isInsurance` AS `isInsurance`,`n`.`insuranceNumber` AS `insuranceNumber`,`n`.`insuranceAmount` AS `insuranceAmount`,`n`.`insuranceExpiration` AS `insuranceExpiration`,`n`.`fideltyApproved` AS `fideltyApproved`,`n`.`fideltyApprovalType` AS `fideltyApprovalType`,`n`.`additionalInformation` AS `additionalInformation`,`n`.`prolinkSignings` AS `prolinkSignings`,`n`.`textReminder` AS `textReminder`,`n`.`preferredContactMethodSMS` AS `preferredContactMethodSMS`,`n`.`preferredContactMethodCall` AS `preferredContactMethodCall`,`n`.`color` AS `color`,`n`.`notaryScore` AS `notaryScore`,`n`.`omit` AS `omit`,`n`.`stripeConnectedAccount` AS `stripeConnectedAccount`,`n`.`stripeAccountVerified` AS `stripeAccountVerified`,`n`.`signature` AS `signature`,`n`.`slug` AS `slug`,`n`.`adminFav` AS `adminFav`,`n`.`eliteUser` AS `eliteUser`,`n`.`facebookID` AS `facebookID`,`n`.`instagramID` AS `instagramID`,`n`.`website` AS `website`,`n`.`car` AS `car`,`n`.`taxID` AS `taxID`,`n`.`ssnOrEin` AS `ssnOrEin`,`n`.`photoApprove` AS `photoApprove`,`n`.`visitCount` AS `visitCount`,`n`.`lastNotaryLogin` AS `lastnotarylogin`,`n`.`smsDisabled` AS `smsDisabled`,`n`.`hideNotary` AS `hideNotary`,`n`.`oneTimeAccess` AS `oneTimeAccess`,`n`.`userID` AS `userID` from (`notaryDetails` `n` join `users` `u`) where (`n`.`userID` = `u`.`userID`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientDetails`
--
ALTER TABLE `clientDetails`
    ADD PRIMARY KEY (`clientID`),
    ADD KEY `userID` (`userID`);

--
-- Indexes for table `notaryDetails`
--
ALTER TABLE `notaryDetails`
    ADD PRIMARY KEY (`notaryID`),
    ADD KEY `userID` (`userID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientDetails`
--
ALTER TABLE `clientDetails`
    MODIFY `clientID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notaryDetails`
--
ALTER TABLE `notaryDetails`
    MODIFY `notaryID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clientDetails`
--
ALTER TABLE `clientDetails`
    ADD CONSTRAINT `clientDetails_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`UserID`);

--
-- Constraints for table `notaryDetails`
--
ALTER TABLE `notaryDetails`
    ADD CONSTRAINT `notaryDetails_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`UserID`);
COMMIT;


ALTER TABLE `users` ADD `city` VARCHAR(100) NOT NULL AFTER `lang`, ADD `state` INT NOT NULL AFTER `city`, ADD `zip` VARCHAR(30) NOT NULL AFTER `state`;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;