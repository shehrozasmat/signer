<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Create Digital signatures and Sign PDF documents online.">
    <meta name="author" content="Simcy Creative">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Notifications | Sign documents online</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}

    <div class="content">
        <div class="page-title">
            <h3>Notifications</h3>
        </div>
        <div class="row">
            <!-- Notification start -->
            <div class="col-md-12 notifications-holder"> 

                @if ( count($requests) > 0 )
                @foreach ( $requests as $request )
                <div class="light-card notification-item unread">
                    <div class="notification-item-image bg-warning btn-round">
                        <span><i class="ion-ios-bell-outline"></i></span>
                    </div>
                    <span class="label label-warning">Important!</span>
                <p>You have been invited you to sign a <a href="{{ url('Document@open').$request->document.'?signingKey='.$request->signing_key }}"><span class="text-primary">document.</span></a>.</p>
                </div>
                @endforeach
                @endif


                @if ( count($notifications) > 0 )
                @foreach ( $notifications as $notification )
                <div class="light-card notification-item">
                    @if ( $notification->type == "accept" )
                    <div class="notification-item-image bg-success btn-round">
                        <span><i class="ion-ios-checkmark"></i></span>
                    </div>
                    @elseif ( $notification->type == "decline" )
                    <div class="notification-item-image bg-danger btn-round">
                        <span><i class="ion-ios-close"></i></span>
                    </div>
                    @else
                    <div class="notification-item-image bg-warning btn-round">
                        <span><i class="ion-ios-bell-outline"></i></span>
                    </div>
                    @endif
                    <div class="pull-right">
                        <span class="delete-notification" data-id="{{ $notification->id }}"><i class="ion-close-round"></i></span>
                    </div>
                    <p>{{ $notification->message }}</p>
                </div>
                @endforeach
                @endif

                @if ( count($sentofferstonotary) > 0 )
                @foreach ( $sentofferstonotary as $offer )
                <div class="light-card notification-item">
                    @if ( $offer->status == 1 )
                    <div class="notification-item-image bg-success btn-round">
                        <span><i class="ion-ios-checkmark"></i></span>
                    </div>
                    @elseif ( $offer->status == 2 )
                    <div class="notification-item-image bg-danger btn-round">
                        <span><i class="ion-ios-close"></i></span>
                    </div>
                    @else
                    <div class="notification-item-image bg-warning btn-round">
                        <span><i class="ion-ios-bell-outline"></i></span>
                    </div>
                    @endif
                    <!-- <div class="pull-right">
                        <span class="delete-offer-notification" data-id="{{ $offer->id }}"><i class="ion-close-round"></i></span>
                    </div> -->
                    <p  data-toggle="modal" data-target="#requestOffer{{ $offer->id }}" data-backdrop="static" data-keyboard="false">You have 1 New Signing Requests Offer</p>

                    <!--Create User Account-->
    <div class="modal fade" id="requestOffer{{ $offer->id }}" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Signing Request Offer Details</h4>
                </div>
                
                    <div class="modal-body">
                        <div class="form-group">
                                <div class="row">
                                <div class="col-md-12 ">
                                    <img src="http://maps.googleapis.com/maps/api/streetview?size=640x320&location={{urlencode($offer->propertyAddress)}}&key=AIzaSyDliqDPl22E0Ifhhv7WAiwWWUFNrXnADzs" width="100%">
                                </div>
                            </div>
                            </div>
                        <div class="form-group">
                        <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:void();" title="Order Number" class="order_detail_section">
                                    <i class="glyphicon glyphicon-sort-by-order"> </i>&nbsp;&nbsp;
                                        {{ $offer->orderNumber }}
                                    </a>
                                </div>
                        
                                <div class="col-md-6">
                                    <a href="javascript:void();" title="Customer Name" class="order_detail_section">
                                    <i class="glyphicon glyphicon-user"> </i>&nbsp;&nbsp;
                                        {{ $offer->borrower1Name }}
                                    </a>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:void();" title="Customer Email" class="order_detail_section">
                                    <i class="glyphicon glyphicon-envelope"> </i>&nbsp;&nbsp;
                                        {{ $offer->borrower1Email }}
                                    </a>
                                </div>
                        
                                <div class="col-md-6">
                                    <a href="javascript:void();" title="Customer Phone" class="order_detail_section">
                                    <i class="glyphicon glyphicon-phone"> </i>&nbsp;&nbsp;
                                        {{ $offer->borrower1Cell }}
                                    </a>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-12">
                                    <a href="javascript:void();" title="Property Address" class="order_detail_section">
                                    <i class="glyphicon glyphicon-map-marker"> </i>&nbsp;&nbsp;
                                        {{ $offer->propertyAddress }}
                                    </a>
                                </div>
                            </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:void();" title="Property Address" class="order_detail_section">
                                    <label>Signing Type</label>&nbsp;&nbsp;
                                       Single Document
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:void();" title="Property Address" class="order_detail_section">
                                    <label>Payment To Notary</label>&nbsp;&nbsp;
                                       $12.5
                                    </a>
                                </div>
                            </div>
                            </div>
                       
                    </div>
                    <div class="modal-footer">
                        
                        
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                        <form class="simcy-form pull-right" action="<?=url("Signingrequests@offerdetails");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data" style="padding: 0 10px;">
                            <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                            <input type="hidden" name="status" value="2" />
                            <input type="hidden" name="rowID" value="{{ $offer->id }}" />
                        <button type="submit" name="offerreactionbtn" value="2" class="btn btn-danger">Reject Request</button>
                        </form>
                        <form class="simcy-form pull-right" action="<?=url("Signingrequests@offerdetails");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                            <input type="hidden" name="status" value="1" />
                            <input type="hidden" name="rowID" value="{{ $offer->id }}" />
                        <button type="submit" name="offerreactionbtn" value="1" class="btn btn-success">Accept Request</button>
                        </form>
                        
                    </div>
                
            </div>

        </div>
    </div>
                </div>
                @endforeach
                @endif

                @if ( count($notifications) < 1 && count($sentofferstonotary) < 1 )
                        <div class="center-notify">
                            <i class="ion-ios-information-outline"></i>
                            <h3>It's empty here!</h3>
                        </div>
                @endif
            </div>
        </div>
    </div>


    
    <!-- footer -->
    {{ view("includes/footer"); }}

    <!-- scripts -->
    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>

    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/app.js"></script>
    <script>
        var deleteNotificationUrl = '<?=url("Notification@delete");?>';
        $(document).ready(function() {
            $(".bubble").hide();
            readNotifications("<?=url("Notification@read");?>");
        });
    </script>
</body>

</html>
