
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Login | Sign documents online</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">

</head>

<body>

    <div class="login-card mb-30">
        <img src="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}" class="img-responsive">
        @if ( $guest AND env('GUEST_SIGNING') == "Enabled" )
        <a class="btn btn-block btn-success m-t-50" href="{{ $signingLink }}">Sign as a Guest</a>
        @endif
        <div class="sign-in">
            <h5 class="mb-30">Sign in to your account </h5>
            <form class="text-left simcy-form" action="<?=url("Auth@signin");?>" data-parsley-validate="" loader="true" method="POST">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Email address</label>
                            <input type="email" class="form-control" name="email" placeholder="Email address" required>
                            <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Password</label>
                            <input type="Password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="pull-left m-t-5"><a href="" target="forgot-password">Forgot password?</a></p>
                            <button class="btn btn-primary pull-right" type="submit" name="login">Sign In</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @if ( env('NEW_ACCOUNTS') == "Enabled" ) 
        <div class="sign-up" style="display: none;">
            <!-- <h5 class="mb-30">Create a free account</h5> -->
            <h5 class="mb-30">Signup as Notary</h5>

            <form class="text-left simcy-form" action="<?=url("Notary@signup");?>" data-parsley-validate="" loader="true" method="POST">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>First name</label>
                            <input type="text" class="form-control" name="fname" placeholder="First name" required>
                            <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                        </div>
                        <div class="col-md-6">
                            <label>Last name</label>
                            <input type="text" class="form-control" name="lname" placeholder="Last name" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Phone #</label>
                            <input type="text" class="form-control" name="phone" placeholder="Phone #" required>
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Mailing Address</label>
                            <input type="text" class="form-control" name="address" placeholder="Mailing address" required>
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>City</label>
                            <input type="text" class="form-control" name="city" placeholder="City" required>
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>State</label>
                            <select class="form-control" name="state" required>
                                <option>Select</option>
                                @if ( count($states) > 0 )
                                    @foreach ( $states as $index => $state )
                                <option value="{{$state->stateID}}">{{$state->stateName}}</option>
                                   @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Zip Code</label>
                            <input type="text" class="form-control" name="zip" placeholder="Zip Code" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Email address</label>
                            <input type="email" class="form-control" name="email" placeholder="Email address" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>New Password</label>
                            <input type="Password" class="form-control" name="password" data-parsley-required="true" data-parsley-minlength="0" data-parsley-error-message="Password is too short!" id="password" placeholder="New Password">
                        </div>
                        <div class="col-md-6">
                            <label>Confirm Password</label>
                            <input type="Password" class="form-control" data-parsley-required="true" data-parsley-equalto="#password" data-parsley-error-message="Passwords don't Match!" placeholder="Confirm Password">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>I am currently a RON Commissioned Notary</label>
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="notaryType" value="Yes">&nbsp;&nbsp;Yes
                              </label>
                            </div>

                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="notaryType" value="No">&nbsp;&nbsp;No
                              </label>
                            </div>

                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="notaryType" value="I plan to be soon">&nbsp;&nbsp;I plan to be soon
                              </label>
                            </div>                 
                        </div>
                    </div>
                </div>
                @if ( env('APP_URL') != "http://localhost" ) 
                 <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            
                              <!-- BEGIN: ReCAPTCHA implementation example. -->
                            <div id="recaptcha-demo" class="g-recaptcha" data-sitekey="6LdCMeoUAAAAAFkO_T9CzlPP6xTQhUjMTyz4qJc1" data-callback="onSuccess">
                            </div>
                            <script nonce="VjRlsAKkBiAvA9Km+RhRwA">
                                var onSuccess = function(response) {
                                    var errorDivs = document.getElementsByClassName("recaptcha-error");
                                    if (errorDivs.length) {
                                        errorDivs[0].className = "";
                                    }
                                    var errorMsgs = document.getElementsByClassName("recaptcha-error-message");
                                    if (errorMsgs.length) {
                                        errorMsgs[0].parentNode.removeChild(errorMsgs[0]);
                                    }
                                };
                            </script>
                            <!-- Optional noscript fallback. -->
                            <noscript>
                                <div style="width: 302px; height: 462px;">
                                    <iframe src="/recaptcha/api/fallback?k=6LdCMeoUAAAAAKescBEbV4IVVO1ZrAuw_lt0zRlW" frameborder="0" scrolling="no"></iframe>
                                    <div>
                                        <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response"></textarea>
                                    </div>
                                </div>
                                <br>
                            </noscript>
                            <!-- END: ReCAPTCHA implementation example. -->
                           
                                                  
                        </div>
                    </div>
                </div>
                 @endif 
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="pull-left m-t-5"><a href="" target="sign-in">Sign In?</a></p>
                            <button class="btn btn-primary pull-right" type="submit">Create account</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      
        @endif
        <div class="forgot-password" style="display: none;">
            <h5 class="mb-30">Forgot password? don't worry, we'll <br>send your a reset link.</h5>
            <form class="text-left simcy-form" action="<?=url("Auth@forgot");?>" method="POST" data-parsley-validate="" loader="true">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Email address</label>
                            <input type="text" class="form-control" name="email" placeholder="Email address" required>
                            <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="pull-left m-t-5"><a href="" target="sign-in">Sign In?</a></p>
                            <button class="btn btn-primary pull-right" type="submit">Send Email</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @if ( env('NEW_ACCOUNTS') == "Enabled" ) 
        <div class="row">
            <div class="col-md-6">
                <div class="m-t-5">
                    <a class="btn btn-block btn-primary-ish m-t-10 sign-up-btn" href="" target="sign-up">
                        Signup
                    </a>
                </div>
            </div>
           
        </div>
        
        
        @endif
        <div class="copyright">
            <p class="text-center"><?=date("Y")?> &copy; <?=env("APP_NAME")?> | All Rights Reserved.</p>
        </div>
    </div>

    <!-- scripts -->
    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>

    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/app.js"></script>
    @if ( env('APP_URL') != "http://localhost" ) 
    <script src="https://www.google.com/recaptcha/api.js?render=6LdCMeoUAAAAAFkO_T9CzlPP6xTQhUjMTyz4qJc1"></script>
    @endif
</body>
 
</html>
