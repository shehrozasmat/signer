<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Create Digital signatures and Sign PDF documents online.">
    <meta name="author" content="Simcy Creative">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Signing Requests | Sign documents online</title>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" />

    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}
    
    <div class="content">
        <div class="page-title">
            <div class="pull-right page-actions lower">
                <button class="btn btn-primary" data-toggle="modal" data-target="#create" data-backdrop="static" data-keyboard="false"><i class="ion-plus-round"></i> New Request</button>
            </div>
            <h3>Signing Requests </h3>
            <p>This is a list of your Signing Requests.</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="light-card table-responsive p-b-3em">
                    <table class="table display companies-list" id="data-table">
                        <thead>
                            <tr>
                                <th class=""></th>
                                <th>Order Number</th>
                                <th>Type of Signing</th>
                                <th>Service Type</th>
                                <th>Borrowe 1 Name</th>
                                <th class="text-center w-70">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ( count($signingsrequests) > 0 )
                            @foreach ( $signingsrequests as $index => $sreq )
                            <tr>
                                <td class="text-center">{{ $index + 1 }}</td>
                                <td><strong>{{ $sreq->orderNumber }}</strong></td>
                                <td>{{ $sreq->typeofSigning }}</td>
                                <td>{{ $sreq->serviceType }}</td>
                                <td>{{ $sreq->borrower1Name }}</td>
                               
                               
                                <td class="text-center">
                                    <div class="dropdown">
                                        <span class="company-action dropdown-toggle" data-toggle="dropdown"><i class="ion-ios-more"></i></span>
                                        <ul class="dropdown-menu" role="menu">
                                            <li role="presentation">
                                                <a href="<?=url("signingrequestdetails/");?>{{ $sreq->orderID }}">Detail</a>

                                                <a href="<?=url("signingrequestoffers/");?>{{ $sreq->orderID }}">Offers</a>

                                                <!-- <a id="{{ $sreq->orderID }}" class="fetch-display-click" data="orderID:{{ $sreq->orderID }}|csrf-token:<?=csrf_token();?>" url="<?=url("Signingrequests@offerview");?>" holder=".update-holder-assign-offer" modal="#assign_to_notary" href="">Assign To Notary</a> -->

                                                <a class="fetch-display-click" data="orderID:{{ $sreq->orderID }}|csrf-token:<?=csrf_token();?>" url="<?=url("Signingrequests@updateview");?>" holder=".update-holder" modal="#update" href="">Edit</a>

                                                <a class="send-to-server-click"  data="orderID:{{ $sreq->orderID }}|csrf-token:<?=csrf_token();?>" url="<?=url("Signingrequests@delete");?>" warning-title="Are you sure?" warning-message="This signings requests data will be deleted." warning-button="Continue" loader="true" href="">Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="9" class="text-center">It's empty here</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <!--Create User Account-->
    <div class="modal fade" id="create" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Signing Request</h4>
                </div>
                <form class="simcy-form"action="<?=url("Signingrequests@create");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <p>Fill in following details to generate an order request.</p>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Order Number</label>
                                    <input type="text" class="form-control" name="orderNumber" placeholder="Order Number" data-parsley-required="true"  data-parsley-required-message="Please fill order number">
                                    <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                </div>
                                <div class="col-md-4">
                                    <label>Type of Signing</label>
                                    <select class="form-control" name="typeofSigning"  data-parsley-required="true"  data-parsley-required-message="Please select type of signing">
                                        <option value="">Select</option>
                                        <option value="Purchase">Purchase</option>
                                        <option value="Refinance">Refinance</option>
                                        <option value="Seller">Seller</option>
                                        <option value="Single Document">Single Document</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Service Type</label>
                                    <select class="form-control" name="serviceType"  data-parsley-required="true" data-parsley-required-message="Please select service type">
                                        <option value="">Select</option>
                                        <option value="Full Digitial RON E-closing">Full Digitial RON E-closing</option>
                                        <option value="Docusign Only (No E-Notary Required)">Docusign Only (No E-Notary Required)</option>
                                    </select>
                                </div>
                            </div>
                            </div>
                        <div class="form-group signing_request_customer">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="label_customer_name">Customer 1 Name</label>
                                    <input type="text" class="form-control input_customer_name" name="borrower1Name" placeholder="Customer 1 Name" data-parsley-required="true" data-parsley-required-message="Please fill Customer 1 Name">
                                   
                                </div>
                                <div class="col-md-4">
                                    <label class="label_customer_email">Customer 1 Email</label>
                                    <input type="email" class="form-control input_customer_email" name="borrower1Email" placeholder="Customer 1 Email" data-parsley-required="true" data-parsley-required-message="Please fill Customer 1 Email">
                                   
                                </div>
                                <div class="col-md-3">
                                    <label class="label_customer_cell_no">Customer 1 Cell #</label>
                                    <input type="text" class="form-control input_customer_cell_no" name="borrower1Cell" placeholder="Customer 1 Cell #" data-parsley-required="true" data-parsley-required-message="Please fill Customer 1 Cell #">
                                   
                                </div>
                                 <div class="col-md-1">
                                   <div class="clone btn btn-primary btn-sm" title="Click to Add More Customers">
                                      <i class="glyphicon glyphicon-plus nomargin"></i>&nbsp;Add
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="more_borrower_clone" id="1"></div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Property Address</label>
                                    <input type="text" class="form-control" name="propertyAddress" placeholder="Property Address" id="autocomplete" onFocus="geolocate()"  data-parsley-required="false"  data-parsley-required-message="Please fill property address">
                                </div>
                                <div class="col-md-6">
                                    <label>City</label>
                                    <input type="text" class="form-control" name="city" placeholder="Property City" id="locality" data-parsley-required="true"  data-parsley-required-message="Please fill property city">
                                   
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>State</label>
                                    <input type="text" class="form-control" name="state" placeholder="Property State" id="administrative_area_level_1"  data-parsley-required="true"  data-parsley-required-message="Please fill property state">
                                </div>
                                <div class="col-md-6">
                                    <label>Zip Code</label>
                                    <input type="text" class="form-control" name="zip" placeholder="Property Zip Code" id="postal_code"  data-parsley-required="true"  data-parsley-required-message="Please fill property zip code">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="street_number">
                        <input type="hidden" id="route">
                        <input type="hidden" id="country">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="createRequest" value="Create Signing Request" class="btn btn-primary">Create Signing Request</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <!-- Update User Modal -->
    <div class="modal fade" id="update" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Signing Request </h4>
                </div>
                <form class="update-holder simcy-form"action="<?=url("Signingrequests@update");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                    <div class="loader-box"><div class="circle-loader"></div></div>
                </form>
            </div>

        </div>
    </div>
    <!-- Assign To Notary Modal -->
    <div class="modal fade" id="assign_to_notary" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Assign To Notary </h4>
                </div>
                <form class="update-holder-assign-offer simcy-form"action="<?=url("Signingrequests@assigntonotary");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                    <div class="loader-box"><div class="circle-loader"></div></div>
                </form>
            </div>

        </div>
    </div>

    

    <!-- footer -->
    {{ view("includes/footer"); }}
    
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    @if ( count($signingsrequests) > 0 )
    <script>
        $(document).ready(function() {
            $('#data-table').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            });
        });
    </script>
    @endif
    <!-- Googlel Map API Code -->

    <script src="<?=url("");?>assets/js/map.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAn7WyR-rrCrWW7Nx3JoJD-8QZGfL9eKe4&libraries=places&callback=initAutocomplete" async defer></script>

    <!-- Googlel Map API Code -->
    <!-- scripts -->
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>


    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>
    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/app.js"></script>

</body>
 
</html>
