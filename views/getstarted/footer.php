</div>

<footer>
    <p class="text-right">&copy; <?=date("Y")?> <?=env("APP_NAME")?> | All Rights Reserved. Ver {{ env("APP_VERSION") }}</p>
</footer>
<script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
<script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
<script src="<?=url("");?>assets/libs/select2/js/select2.min.js"></script>
<script src="<?=url("");?>assets/js/simcify.min.js"></script>
<script src="<?=url("");?>assets/libs/clipboard/clipboard.min.js"></script>

