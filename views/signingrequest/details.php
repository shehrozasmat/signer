<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Create Digital signatures and Sign PDF documents online.">
    <meta name="author" content="Simcy Creative">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Signing Requests | Sign documents online</title>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" />

    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">

</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}
    
    <div class="content">
        <div class="page-title">
            <div class="pull-right page-actions lower">
                <button class="btn btn-primary" data-toggle="modal" data-target="#create" data-backdrop="static" data-keyboard="false"><i class="ion-plus-round"></i> New Request</button>
            </div>
            <h3>Signing Requests </h3>
            <p>This is a list of your Signing Requests.</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="light-card p-b-3em">
                <div class="accordion" id="accordionExample275">
                  <div class="card z-depth-0 bordered">
                    <div class="card-header" id="headingOne2">
                      <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne2"
                          aria-expanded="true" aria-controls="collapseOne2">
                            <span class="pull-left">
                                <i class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;
                                Signing Request Details
                            </span>
                          <span class="pull-right">
                               <i class="glyphicon glyphicon-edit"></i>
                          </span>
                         
                        </button>
                      </h5>
                    </div>

                    <div id="collapseOne2" class="collapse in" aria-labelledby="headingOne2"
                      data-parent="#accordionExample275">
                      <div class="card-body">
                        <img src="http://maps.googleapis.com/maps/api/streetview?size=640x320&location={{urlencode($signingsrequests->propertyAddress)}}&key=AIzaSyDliqDPl22E0Ifhhv7WAiwWWUFNrXnADzs" width="100%">
                        <div class="container-fluid">
                            
                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-12 ">
                                    <a href="javascript:void();" title="Order Number" class="order_detail_section">
                                    <i class="glyphicon glyphicon-sort-by-order"> </i>&nbsp;&nbsp;
                                        {{ $signingsrequests->orderNumber }}
                                    </a>
                                </div>
                            </div>
                            </div> 
                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-12">
                                    <a href="javascript:void();" title="Customer Name" class="order_detail_section">
                                    <i class="glyphicon glyphicon-user"> </i>&nbsp;&nbsp;
                                        {{ $signingsrequests->borrower1Name }}
                                    </a>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-12">
                                    <a href="javascript:void();" title="Customer Email" class="order_detail_section">
                                    <i class="glyphicon glyphicon-envelope"> </i>&nbsp;&nbsp;
                                        {{ $signingsrequests->borrower1Email }}
                                    </a>
                                </div>
                            </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-12">
                                    <a href="javascript:void();" title="Customer Phone" class="order_detail_section">
                                    <i class="glyphicon glyphicon-phone"> </i>&nbsp;&nbsp;
                                        {{ $signingsrequests->borrower1Cell }}
                                    </a>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                <div class="col-md-12">
                                    <a href="javascript:void();" title="Property Address" class="order_detail_section">
                                    <i class="glyphicon glyphicon-map-marker"> </i>&nbsp;&nbsp;
                                        {{ $signingsrequests->propertyAddress }}
                                    </a>
                                </div>
                            </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>
            </div>

        </div>
<hr>

 <div class="row">
            <div class="col-md-12">
                <div class="light-card p-b-3em">
                <div class="accordion" id="accordionExample275">
                  <div class="card z-depth-0 bordered">
                    <div class="card-header" id="headingOne2">
                      <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne2"
                          aria-expanded="true" aria-controls="collapseOne2">
                            <span class="pull-left">
                                <i class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;
                                Upload Documents
                            </span>
                          
                         
                        </button>
                      </h5>
                    </div>
                    <div id="collapseOne2" class="collapse in" aria-labelledby="headingOne2"
                      data-parent="#accordionExample275">
                      <div class="card-body">
                        
       <div class="container-fluid">
            <form class="simcy-form"action="<?=url("Signingrequests@uploadfile");?>" data-parsley-validate="" loader="true" method="POST">
                    <div class="modal-body">
                        <p>Only PDF<?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>, Word, Excel and Power Point <?php } ?> allowed.</p>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>File name</label>
                                    <input type="text" class="form-control" name="name" placeholder="File name" data-parsley-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                             <div class="row">
                                <div class="col-md-12">
                                    <label>Choose file</label>
                                    <input type="file" name="file" class="dropify" data-parsley-required="true" data-allowed-file-extensions="pdf <?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>doc docx ppt pptx xls xlsx <?php } ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="folder" value="1">
                        <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                        <input type="hidden" name="orderID" value="{{ $signingsrequests->orderID }}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload file</button>
                    </div>
                </form>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Document</th>
                                    <th>Uploaded On</th>
                                    <th>Mark Up</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ( count($requestDoctumnets) > 0 )
                                    @foreach ( $requestDoctumnets as $index => $d )
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $d->name }}</td>
                                        <td>{{ $d->uploaded_on }}</td>
                                        <td>
                                            @if (!empty($d->template_fields))
                                            <i class="glyphicon glyphicon-ok" style="color: #5bea5b"></i>
                                            @endif
                                            @if (empty($d->template_fields))
                                            <i class="glyphicon glyphicon-remove" style="color: red"></i>
                                            @endif
                                            
                                        </td>
                                        <td>
                                            <a href="<?=url("markup/");?>{{ $d->document_key }}" target="_blank" title="Mark Up Document">
                                                <i class="glyphicon glyphicon-check"></i>
                                            </a>&nbsp&nbsp
                                            <a href="<?=url("document/");?>{{ $d->document_key }}" target="_blank" title="View Document">
                                                <i class="glyphicon glyphicon-eye-open"></i>
                                                
                                            </a>

                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            
                        </table>
                    </div>
                </div>
       </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>
            </div>

        </div>
<hr>

        <div class="row">
            <div class="col-md-12">
                <div class="light-card p-b-3em">
                <div class="accordion" id="accordionExample275">
                  <div class="card z-depth-0 bordered">
                    <div class="card-header" id="headingOne2">
                      <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne2"
                          aria-expanded="true" aria-controls="collapseOne2">
                            <span class="pull-left">
                                <i class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;
                                Line Items
                            </span>
                          
                         
                        </button>
                      </h5>
                    </div>
                    <div id="collapseOne2" class="collapse in" aria-labelledby="headingOne2"
                      data-parent="#accordionExample275">
                      <div class="card-body">
                       
                        <div class="container-fluid">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <b>Drain Pipe Removal</b>
                                    </div>
                                    <div class="col-md-3">
                                        <b>$14,000</b>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <b>Drain Pipe Removal</b>
                                    </div>
                                    <div class="col-md-3">
                                        <b>$14,000</b>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <b>Drain Pipe Removal</b>
                                    </div>
                                    <div class="col-md-3">
                                        <b>$14,000</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>
            </div>

        </div>

    </div>


    <!-- footer -->
    {{ view("includes/footer"); }}
    
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    @if ( count($customers) > 0 )
    <script>
        $(document).ready(function() {
            $('#data-table').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            });
        });
    </script>
    @endif
    <!-- Googlel Map API Code -->

    <script src="<?=url("");?>assets/js/map.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAn7WyR-rrCrWW7Nx3JoJD-8QZGfL9eKe4&libraries=places&callback=initAutocomplete" async defer></script>

    <!-- Googlel Map API Code -->
    <!-- scripts -->
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>
    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/app.js"></script>
</body>
 
</html>
