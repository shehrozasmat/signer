<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Create Digital signatures and Sign PDF documents online.">
    <meta name="author" content="Simcy Creative">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Signing Requests | Sign documents online</title>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" />

    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS for datetimepicker -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

    <!-- Latest compiled and minified CSS for selectpicker-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css">

</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}
    
    <div class="content">
        <div class="page-title">
            <div class="pull-right page-actions lower">
                <a href="javascript:void(0);" class="btn btn-primary fetch-display-click" id="{{ $id }}"  data="orderID:{{ $id }}|csrf-token:<?=csrf_token();?>" url="<?=url("Signingrequests@offerview");?>" holder=".update-holder-assign-offer" modal="#assign_to_notary"><i class="ion-plus-round"></i> New Request</a>
            </div>
            <h3>Signing Requests Offers</h3>
            <p>This is a list of your Signing Requests Offers.</p>
        </div>

 <div class="row">
            <div class="col-md-12">
                <div class="light-card p-b-3em">
                <div class="accordion" id="accordionExample275">
                  <div class="card z-depth-0 bordered">
                    <div class="card-header" id="headingOne2">
                      <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne2"
                          aria-expanded="true" aria-controls="collapseOne2">
                            <span class="pull-left">
                                <i class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;
                                Signing Requests Offers
                            </span>
                          
                         
                        </button>
                      </h5>
                    </div>
                    <div id="collapseOne2" class="collapse in" aria-labelledby="headingOne2"
                      data-parent="#accordionExample275">
                      <div class="card-body">
                        
       <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12 table-responsive">
                    <table class="table display companies-list" id="data-table">
                                        <thead>
                                            <tr>
                                                <th>Sr.</th>
                                                <th>Notary</th>
                                                <th>Response</th>
                                                <th>ResponseTime</th>
                                                <th>Status</th>
                                                <th>Send On</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @if ( count($signingoffers) > 0 )
                                                @foreach ( $signingoffers as $index => $offer )
                                            <tr>
                                                <td>{{ $index + 1 }}</td>
                                                <td>{{ $offer->fname." ".$offer->lname; }}</td>
                                                <td>{{ $offer->response }}</td>
                                                <td>{{ date("d M, Y h:i:s",strtotime($offer->responseTime)); }}</td>
                                                <td>
                                                    @if ($offer->status == 0)
                                                            Awaiting Response
                                                    @endif
                                                    @if ($offer->status == 1)
                                                            Accepted
                                                    @endif
                                                    @if ($offer->status == 2)
                                                        Rejected
                                                    @endif 
                                                    @if ($offer->status == 3)
                                                        Assigned
                                                    @endif
                                                </td>
                                                <td>{{ date("d M, Y h:i:s",strtotime($offer->dated)); }}</td>
                                                <td>
                                                    @if ($offer->status == 1)
                                                            <a href="<?=url("signingrequestassigned/");?>{{ $offer->id }}" onclick="return confirm('Are You Sure You Want To Assign Signing Request To This Notary?')" class="btn btn-success">
                                                                Assign Offer
                                                            </a>
                                                    @endif
                                                    @if ($offer->status == 3)
                                                            <a href="<?=url("signingrequestnotification/");?>{{ $offer->id }}" onclick="return confirm('Are You Sure You Want To Send Notification to Borrower and Notary?')" class="btn btn-info">
                                                                Send Notification
                                                            </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                    </div>
                </div>
       </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>
            </div>

        </div>

    <!-- Assign To Notary Modal -->
    <div class="modal fade" id="assign_to_notary" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Assign To Notary </h4>
                </div>
                <form class="update-holder-assign-offer simcy-form"action="<?=url("Signingrequests@assigntonotary");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                    <div class="loader-box"><div class="circle-loader"></div></div>
                </form>
            </div>

        </div>
    </div>

    </div>


    <!-- footer -->
    {{ view("includes/footer"); }}
    
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    @if ( count($signingoffers) > 0 )
    <script>
        $(document).ready(function() {
            $('#data-table').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            });
        });
    </script>
    @endif
    <!-- Googlel Map API Code -->


    <!-------------Datetime Picker--------------------------->

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    
    
    <script type="text/javascript">
         $('#datetimepicker1').datetimepicker();
    </script>

  </script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/i18n/defaults-en_US.js"></script>

    <script src="<?=url("");?>assets/js/map.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAn7WyR-rrCrWW7Nx3JoJD-8QZGfL9eKe4&libraries=places&callback=initAutocomplete" async defer></script>

    <!-- Googlel Map API Code -->
    <!-- scripts -->
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>
    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/app.js"></script>
</body>
 
</html>
