<div class="modal-body">

                            <div class="loader_assign_modal_box">
                                <div class="circle-loader"></div>
                            </div>
                            <div class="form-group schedule_date_time_picker">
                            <div class="row">
                                
                                <div class="col-md-6 col-md-offset-3">
                                    <label>Select Date & Time</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                     <input type='text'  name="RequestedAppointmentDate"  class="form-control"  data-parsley-required="true"  data-parsley-required-message="Please select Time" />
                                     <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                     </span>
                                  </div>
                                </div>
                                
                            </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                       <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                       <input type="hidden" name="signing_request_order_id" id="signing_request_order_id" value="{{ $orderID }}" />
                       
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="assignToNotary" value="Assign" class="btn btn-primary">Send Offers</button>
                    </div>

 