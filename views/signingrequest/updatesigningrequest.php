                   
                    <div class="modal-body">
                        <p>Update Signing Request info. 11<?php echo $signingsrequests->extraBorrower;?></p>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Order Number</label>
                                    <input type="text" class="form-control" name="orderNumber" placeholder="Order Number" value="{{ $signingsrequests->orderNumber }}" data-parsley-required="true"  data-parsley-required-message="Please fill order number">
                                    <input type="hidden" name="csrf-token" value="{{ csrf_token(); }}" />
                                    <input type="hidden" name="orderID" value="{{ $signingsrequests->orderID }}" />
                                </div>
                                <div class="col-md-4">
                                    <label>Type of Signing</label>
                                    <select class="form-control" name="typeofSigning"  data-parsley-required="true"  data-parsley-required-message="Please select type of signing">
                                        <option value="">Select</option>
                                        <option value="Purchase"
                                         @if( !empty( $signingsrequests->typeofSigning == "Purchase" ) ) selected @endif >Purchase</option>
                                        <option value="Refinance"
                                        @if( !empty( $signingsrequests->typeofSigning == "Refinance" ) ) selected @endif >Refinance</option>
                                        <option value="Seller"
                                        @if( !empty( $signingsrequests->typeofSigning == "Seller" ) ) selected @endif >Seller</option>
                                        <option value="Single Document"
                                        @if( !empty( $signingsrequests->typeofSigning == "Single Document" ) ) selected @endif >Single Document</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Service Type</label>
                                    <select class="form-control" name="serviceType"  data-parsley-required="true" data-parsley-required-message="Please select service type">
                                        <option value="">Select</option>
                                        <option value="Full Digitial RON E-closing"
                                        @if( !empty( $signingsrequests->serviceType == "Full Digitial RON E-closing")) selected @endif >Full Digitial RON E-closing</option>
                                        <option value="Docusign Only (No E-Notary Required)"
                                        @if( !empty( $signingsrequests->serviceType == "Docusign Only (No E-Notary Required)")) selected @endif 
                                        >Docusign Only (No E-Notary Required)</option>
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                         <div class="form-group signing_request_customer_update">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="label_customer_name">Customer 1 Name</label>
                                    <input type="text" class="form-control input_customer_name" name="borrowe1Name" placeholder="Customer 1 Name" value="{{ $signingsrequests->borrowe1Name }}" data-parsley-required="true" data-parsley-required-message="Please fill Customer 1 Name">
                                   
                                </div>
                                <div class="col-md-4">
                                    <label class="label_customer_email">Customer 1 Email</label>
                                    <input type="email" class="form-control input_customer_email" name="borrower1Email" placeholder="Customer 1 Email" value="{{ $signingsrequests->borrower1Email }}" data-parsley-required="true" data-parsley-required-message="Please fill Customer 1 Email">
                                   
                                </div>
                                <div class="col-md-3">
                                    <label class="label_customer_cell_no">Customer 1 Cell #</label>
                                    <input type="text" class="form-control input_customer_cell_no" name="borrower1Cell" placeholder="Customer 1 Cell #" value="{{ $signingsrequests->borrower1Cell }}" data-parsley-required="true" data-parsley-required-message="Please fill Customer 1 Cell #">
                                   
                                </div>
                                 <div class="col-md-1">
                                   <div class="clone2 btn btn-primary btn-sm" title="Click to Add More Customers">
                                      <i class="glyphicon glyphicon-plus nomargin"></i>&nbsp;Add
                                   </div>
                                </div>
                            </div>
                        </div>
                      <?php
                      if(!empty($signingsrequests->extraBorrower)){
                         $extraBorrower = explode(" | ", $signingsrequests->extraBorrower);
                       foreach($extraBorrower as $ex){
                            $customer_data = explode(",",$ex); ?> 

                        <div class="form-group more_customer_sections signing_request_customer">
    <div class="row">
        <div class="col-md-4">
            <label class="label_customer_name">Customer 2 Name</label>
            <input type="text" class="form-control input_customer_name" name="borrowerName[]" placeholder="Customer 2 Name" data-parsley-required="true" value="{{ $customer_data[0] }}" data-parsley-required-message="Please fill Customer 2 Name">
        </div>
        <div class="col-md-4">
            <label class="label_customer_email">Customer 2 Email</label>
            <input type="email" class="form-control input_customer_email" name="borrower2Email[]" placeholder="Customer 2 Email" data-parsley-required="true" value="{{ $customer_data[1] }}" data-parsley-required-message="Please fill Customer 2 Email">
        </div>
        <div class="col-md-3">
            <label class="label_customer_cell_no">Customer 2 Cell #</label>
            <input type="text" class="form-control input_customer_cell_no" name="borrowercell[]" placeholder="Customer 2 Cell #" data-parsley-required="true" value="{{ $customer_data[2] }}" data-parsley-required-message="Please fill Customer 2 Cell #">
        </div>
        <div class="col-md-1">
            <div class="del enabled text-right btn btn-sm btn-danger" id="2"><i class=" glyphicon glyphicon-minus nomargin"></i>Remove</div>
        </div>
    </div>
</div>
<?php }
}?>


                        <div class="more_borrower_clone_update" id="1"></div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Property Address</label>
                                    <input type="text" class="form-control" name="propertyAddress" placeholder="Property Address" id="autocomplete" onFocus="geolocate()"  data-parsley-required="true"  data-parsley-required-message="Please fill property address">
                                </div>
                                <div class="col-md-6">
                                    <label>City</label>
                                    <input type="text" class="form-control" name="city" placeholder="Property City" id="locality" data-parsley-required="true"  data-parsley-required-message="Please fill property city">
                                   
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>State</label>
                                    <input type="text" class="form-control" name="state" placeholder="Property State" id="administrative_area_level_1"  data-parsley-required="true"  data-parsley-required-message="Please fill property state">
                                </div>
                                <div class="col-md-6">
                                    <label>Zip Code</label>
                                    <input type="text" class="form-control" name="zip" placeholder="Property Zip Code" id="postal_code"  data-parsley-required="true"  data-parsley-required-message="Please fill property zip code">
                                   
                                </div>
                            </div>
                        </div>
                      
                       
                    </div>
                    <div class="modal-footer">
                         <input type="hidden" id="street_number">
                        <input type="hidden" id="route">
                        <input type="hidden" id="country">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                    <script type="text/javascript">
                        croppify();
                    </script>