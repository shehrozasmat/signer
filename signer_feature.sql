-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2020 at 12:08 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `signer_feature`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `message` text NOT NULL,
  `time_` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `file` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientdetails`
--

CREATE TABLE `clientdetails` (
  `clientID` int(11) NOT NULL,
  `company` varchar(50) NOT NULL,
  `businessPhone` varchar(20) DEFAULT NULL,
  `afterHoursPhone` varchar(15) DEFAULT NULL,
  `birthDay` int(11) DEFAULT NULL,
  `birthMonth` int(11) DEFAULT NULL,
  `defaultPayment` varchar(100) DEFAULT NULL,
  `printFee` varchar(100) DEFAULT NULL,
  `noSignFee` varchar(100) DEFAULT NULL,
  `redrawFee` varchar(100) DEFAULT NULL,
  `cancelledFee` varchar(100) DEFAULT NULL,
  `addtionalTerms` varchar(100) DEFAULT NULL,
  `shippingMethod` varchar(100) DEFAULT NULL,
  `isShippingAccountNumber` tinyint(1) DEFAULT NULL,
  `shippingAccountNumber` varchar(100) DEFAULT NULL,
  `shippingAccountLabel` varchar(100) DEFAULT NULL,
  `orderNotificationEmails` tinyint(1) DEFAULT NULL,
  `serviceType` int(11) DEFAULT NULL,
  `customTierFee` varchar(10) DEFAULT NULL,
  `emailDocsPrice` varchar(50) DEFAULT NULL,
  `sellerSigningsPrice` varchar(50) DEFAULT NULL,
  `firstAndSecondPrice` varchar(50) DEFAULT NULL,
  `invoiceTo` varchar(10) DEFAULT NULL,
  `firstVisitonDashboard` int(11) DEFAULT NULL,
  `firstVisitonSearch` int(11) DEFAULT NULL,
  `weeklyInvoice` tinyint(1) DEFAULT NULL,
  `emailPlan` varchar(15) DEFAULT NULL,
  `noAttachAssignOrder` tinyint(1) DEFAULT NULL,
  `blockProfile` tinyint(1) DEFAULT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clientdetails`
--

INSERT INTO `clientdetails` (`clientID`, `company`, `businessPhone`, `afterHoursPhone`, `birthDay`, `birthMonth`, `defaultPayment`, `printFee`, `noSignFee`, `redrawFee`, `cancelledFee`, `addtionalTerms`, `shippingMethod`, `isShippingAccountNumber`, `shippingAccountNumber`, `shippingAccountLabel`, `orderNotificationEmails`, `serviceType`, `customTierFee`, `emailDocsPrice`, `sellerSigningsPrice`, `firstAndSecondPrice`, `invoiceTo`, `firstVisitonDashboard`, `firstVisitonSearch`, `weeklyInvoice`, `emailPlan`, `noAttachAssignOrder`, `blockProfile`, `userID`) VALUES
(1, 'test company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18),
(2, 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30);

-- --------------------------------------------------------

--
-- Stand-in structure for view `clientusers`
-- (See below for the actual view)
--
CREATE TABLE `clientusers` (
`clientID` int(11)
,`company` varchar(50)
,`businessPhone` varchar(20)
,`afterHoursPhone` varchar(15)
,`birthDay` int(11)
,`birthMonth` int(11)
,`defaultPayment` varchar(100)
,`printFee` varchar(100)
,`noSignFee` varchar(100)
,`redrawFee` varchar(100)
,`cancelledFee` varchar(100)
,`addtionalTerms` varchar(100)
,`shippingMethod` varchar(100)
,`isShippingAccountNumber` tinyint(1)
,`shippingAccountNumber` varchar(100)
,`shippingAccountLabel` varchar(100)
,`orderNotificationEmails` tinyint(1)
,`serviceType` int(11)
,`customTierFee` varchar(10)
,`emailDocsPrice` varchar(50)
,`sellerSigningsPrice` varchar(50)
,`firstAndSecondPrice` varchar(50)
,`invoiceTo` varchar(10)
,`firstvisitondashboard` int(11)
,`firstvisitonsearch` int(11)
,`weeklyInvoice` tinyint(1)
,`emailPlan` varchar(15)
,`noAttachAssignOrder` tinyint(1)
,`blockProfile` tinyint(1)
,`userID` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `reminders` enum('On','Off') NOT NULL DEFAULT 'On'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `phone`, `reminders`) VALUES
(0, 'System Users', '', '', 'On'),
(1, 'Simcy Creative', 'demo@simcycreative.com', '+254720783834', 'On');

-- --------------------------------------------------------

--
-- Table structure for table `departmentmembers`
--

CREATE TABLE `departmentmembers` (
  `id` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `member` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `label` text NOT NULL,
  `value` text NOT NULL,
  `type` enum('custom','input','stamp') NOT NULL DEFAULT 'custom'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `folder` int(11) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `uploaded_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `name` varchar(256) NOT NULL,
  `filename` varchar(256) NOT NULL,
  `extension` varchar(16) NOT NULL DEFAULT 'pdf',
  `size` int(11) NOT NULL DEFAULT 0,
  `document_key` varchar(32) NOT NULL,
  `status` enum('Unsigned','Signed') NOT NULL DEFAULT 'Unsigned',
  `editted` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_template` enum('No','Yes') NOT NULL DEFAULT 'No',
  `template_fields` text DEFAULT NULL,
  `sign_reason` text DEFAULT NULL,
  `accessibility` enum('Everyone','Departments','Only Me') NOT NULL DEFAULT 'Everyone',
  `public_permissions` enum('read_only','sign_edit','disabled') NOT NULL DEFAULT 'sign_edit',
  `departments` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `company`, `folder`, `uploaded_by`, `uploaded_on`, `name`, `filename`, `extension`, `size`, `document_key`, `status`, `editted`, `is_template`, `template_fields`, `sign_reason`, `accessibility`, `public_permissions`, `departments`) VALUES
(1, 1, 1, 1, '2020-04-17 16:19:26', 'test', 'WrJCbxyJMnEN9Cd1GC3nJOvC3zXVCsIA.pdf', 'pdf', 14, '3eolVNFrIgm28iA4ffVTOXJ4UPPWlfPM', 'Unsigned', 'No', 'Yes', NULL, NULL, 'Everyone', 'sign_edit', NULL),
(2, 1, 1, 1, '2020-04-17 16:24:39', 'MS Word', 'NZa0pb4bPsEKhdqnsR66xEPvy7Kjz0SP.pdf', 'pdf', 801, 'Nx4SdaFjQm9MMF2YozdryRNOrgcb2znJ', 'Unsigned', 'No', 'Yes', NULL, NULL, 'Everyone', 'sign_edit', NULL),
(3, 1, 1, 1, '2020-04-17 16:35:28', 'tst', 'bZBCgqxwgkHgfWKVSN9J1Jnz7djLdrwN.pdf', 'pdf', 801, 'pnQ59Iq1ScAU0Ck9gHvqzjYvEZ48qJRx', 'Unsigned', 'No', 'Yes', NULL, NULL, 'Everyone', 'sign_edit', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `name` varchar(256) NOT NULL,
  `folder` int(11) NOT NULL DEFAULT 1,
  `accessibility` enum('Everyone','Departments','Only Me') NOT NULL DEFAULT 'Everyone',
  `departments` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`id`, `company`, `created_by`, `created_on`, `name`, `folder`, `accessibility`, `departments`, `password`) VALUES
(1, 1, 1, '2019-01-15 10:00:15', 'public_folder', 1, 'Everyone', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `file` varchar(32) NOT NULL,
  `activity` text NOT NULL,
  `time_` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `type` enum('default','success','danger') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `company`, `file`, `activity`, `time_`, `type`) VALUES
(1, 1, '3eolVNFrIgm28iA4ffVTOXJ4UPPWlfPM', 'Template uploaded by <span class=\"text-primary\">Daniel Kimeli</span>.', '2020-04-17 16:19:26', 'default'),
(2, 1, 'Nx4SdaFjQm9MMF2YozdryRNOrgcb2znJ', 'Template uploaded by <span class=\"text-primary\">Daniel Kimeli</span>.', '2020-04-17 16:24:39', 'default'),
(3, 1, 'pnQ59Iq1ScAU0Ck9gHvqzjYvEZ48qJRx', 'Template uploaded by <span class=\"text-primary\">Daniel Kimeli</span>.', '2020-04-17 16:35:28', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `notarydetails`
--

CREATE TABLE `notarydetails` (
  `notaryID` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `phone2` varchar(15) DEFAULT NULL,
  `phone3` varchar(15) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `introduction` text DEFAULT NULL,
  `canReceiveDocViaEmail` tinyint(1) DEFAULT NULL,
  `haveLaserPrinter` tinyint(1) DEFAULT NULL,
  `canPrintLegalSize` tinyint(1) DEFAULT NULL,
  `haveADualTrayPrinter` tinyint(1) DEFAULT NULL,
  `foreignLanguages` tinyint(1) DEFAULT NULL,
  `notaryExperience` varchar(100) DEFAULT NULL,
  `haveExperienceInSigning` tinyint(1) DEFAULT NULL,
  `canContactReference` tinyint(1) DEFAULT NULL,
  `signingsPerformed` varchar(100) DEFAULT NULL,
  `loanSigningMethod` varchar(100) DEFAULT NULL,
  `notaryComissionNumber` varchar(50) DEFAULT NULL,
  `notaryComissionExpiration` varchar(50) DEFAULT NULL,
  `isBounder` tinyint(1) DEFAULT NULL,
  `boundNumber` varchar(50) DEFAULT NULL,
  `boundAmount` varchar(50) DEFAULT NULL,
  `boundExpiration` varchar(50) DEFAULT NULL,
  `isInsurance` tinyint(1) DEFAULT NULL,
  `insuranceNumber` varchar(50) DEFAULT NULL,
  `insuranceAmount` varchar(50) DEFAULT NULL,
  `insuranceExpiration` varchar(50) DEFAULT NULL,
  `fideltyApproved` tinyint(1) DEFAULT NULL,
  `fideltyApprovalType` varchar(100) DEFAULT NULL,
  `additionalInformation` varchar(100) DEFAULT NULL,
  `prolinkSignings` int(11) DEFAULT NULL,
  `textReminder` tinyint(1) DEFAULT NULL,
  `preferredContactMethodSMS` tinyint(1) DEFAULT NULL,
  `preferredContactMethodCall` tinyint(1) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `notaryScore` int(11) DEFAULT NULL,
  `omit` tinyint(1) DEFAULT NULL,
  `stripeConnectedAccount` varchar(100) DEFAULT NULL,
  `stripeAccountVerified` tinyint(1) DEFAULT NULL,
  `signature` varchar(100) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `adminFav` int(11) DEFAULT NULL,
  `eliteUser` int(11) DEFAULT NULL,
  `facebookID` varchar(250) DEFAULT NULL,
  `instagramID` varchar(250) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `car` varchar(255) DEFAULT NULL,
  `taxID` varchar(255) DEFAULT NULL,
  `ssnOrEin` varchar(255) DEFAULT NULL,
  `photoApprove` tinyint(1) DEFAULT NULL,
  `visitCount` int(11) DEFAULT NULL,
  `lastNotaryLogin` datetime DEFAULT NULL,
  `smsDisabled` tinyint(4) DEFAULT NULL,
  `hideNotary` tinyint(4) DEFAULT NULL,
  `oneTimeAccess` tinyint(1) DEFAULT NULL,
  `notaryType` varchar(100) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notarydetails`
--

INSERT INTO `notarydetails` (`notaryID`, `phone`, `phone2`, `phone3`, `video`, `introduction`, `canReceiveDocViaEmail`, `haveLaserPrinter`, `canPrintLegalSize`, `haveADualTrayPrinter`, `foreignLanguages`, `notaryExperience`, `haveExperienceInSigning`, `canContactReference`, `signingsPerformed`, `loanSigningMethod`, `notaryComissionNumber`, `notaryComissionExpiration`, `isBounder`, `boundNumber`, `boundAmount`, `boundExpiration`, `isInsurance`, `insuranceNumber`, `insuranceAmount`, `insuranceExpiration`, `fideltyApproved`, `fideltyApprovalType`, `additionalInformation`, `prolinkSignings`, `textReminder`, `preferredContactMethodSMS`, `preferredContactMethodCall`, `color`, `notaryScore`, `omit`, `stripeConnectedAccount`, `stripeAccountVerified`, `signature`, `slug`, `adminFav`, `eliteUser`, `facebookID`, `instagramID`, `website`, `car`, `taxID`, `ssnOrEin`, `photoApprove`, `visitCount`, `lastNotaryLogin`, `smsDisabled`, `hideNotary`, `oneTimeAccess`, `notaryType`, `userID`) VALUES
(1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'YES', 17),
(2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'YES', 19),
(3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 20),
(4, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 25),
(5, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 28);

-- --------------------------------------------------------

--
-- Stand-in structure for view `notaryusers`
-- (See below for the actual view)
--
CREATE TABLE `notaryusers` (
`notaryID` int(11)
,`phone` varchar(20)
,`phone2` varchar(15)
,`phone3` varchar(15)
,`video` varchar(200)
,`introduction` text
,`canReceiveDocViaEmail` tinyint(1)
,`haveLaserPrinter` tinyint(1)
,`canPrintLegalSize` tinyint(1)
,`haveADualTrayPrinter` tinyint(1)
,`foreignLanguages` tinyint(1)
,`notaryExperience` varchar(100)
,`haveExperienceInSigning` tinyint(1)
,`canContactReference` tinyint(1)
,`signingsPerformed` varchar(100)
,`loanSigningMethod` varchar(100)
,`notaryComissionNumber` varchar(50)
,`notaryComissionExpiration` varchar(50)
,`isBounder` tinyint(1)
,`boundNumber` varchar(50)
,`boundAmount` varchar(50)
,`boundExpiration` varchar(50)
,`isInsurance` tinyint(1)
,`insuranceNumber` varchar(50)
,`insuranceAmount` varchar(50)
,`insuranceExpiration` varchar(50)
,`fideltyApproved` tinyint(1)
,`fideltyApprovalType` varchar(100)
,`additionalInformation` varchar(100)
,`prolinkSignings` int(11)
,`textReminder` tinyint(1)
,`preferredContactMethodSMS` tinyint(1)
,`preferredContactMethodCall` tinyint(1)
,`color` varchar(100)
,`notaryScore` int(11)
,`omit` tinyint(1)
,`stripeConnectedAccount` varchar(100)
,`stripeAccountVerified` tinyint(1)
,`signature` varchar(100)
,`slug` varchar(150)
,`adminFav` int(11)
,`eliteUser` int(11)
,`facebookID` varchar(250)
,`instagramID` varchar(250)
,`website` varchar(255)
,`car` varchar(255)
,`taxID` varchar(255)
,`ssnOrEin` varchar(255)
,`photoApprove` tinyint(1)
,`visitCount` int(11)
,`lastnotarylogin` datetime
,`smsDisabled` tinyint(4)
,`hideNotary` tinyint(4)
,`oneTimeAccess` tinyint(1)
,`userID` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `time_` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` int(11) NOT NULL,
  `type` enum('decline','accept','warning') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `subject` varchar(256) NOT NULL,
  `days` int(11) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `signing_key` varchar(256) NOT NULL,
  `document` varchar(256) NOT NULL,
  `company` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `positions` text NOT NULL,
  `chain_emails` text DEFAULT NULL,
  `chain_positions` text DEFAULT NULL,
  `sender_note` text DEFAULT NULL,
  `send_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` datetime DEFAULT NULL,
  `status` enum('Pending','Signed','Declined','Cancelled') NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `signingsrequests`
--

CREATE TABLE `signingsrequests` (
  `orderID` int(11) NOT NULL,
  `clientID` int(11) DEFAULT NULL,
  `notaryID` int(11) DEFAULT NULL,
  `propertyAddress` varchar(100) DEFAULT NULL,
  `typeofSigning` varchar(200) DEFAULT NULL,
  `serviceType` varchar(255) DEFAULT NULL,
  `borrowe1Name` varchar(100) DEFAULT NULL,
  `borrower1Email` varchar(50) DEFAULT NULL,
  `borrower1Cell` varchar(20) DEFAULT NULL,
  `borrowe2Name` varchar(100) DEFAULT NULL,
  `borrower2Email` varchar(50) DEFAULT NULL,
  `borrower2Cell` varchar(20) DEFAULT NULL,
  `orderNumber` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `fee` varchar(10) DEFAULT NULL,
  `orderDate` datetime DEFAULT current_timestamp(),
  `clientFee` varchar(10) DEFAULT NULL,
  `isScheduled` tinyint(1) DEFAULT NULL,
  `scheduleOption` varchar(10) DEFAULT NULL,
  `scheduleTime` datetime DEFAULT NULL,
  `scheduleTimeCustom` varchar(20) DEFAULT NULL,
  `scheduleOptionNotes` varchar(200) DEFAULT NULL,
  `RequestedAppointmentDate` timestamp NULL DEFAULT NULL,
  `signingType` varchar(50) DEFAULT NULL,
  `methodOfDelivery` varchar(50) DEFAULT NULL,
  `EstimatedDocsAvailable` varchar(50) DEFAULT NULL,
  `EstimatedPackageSize` varchar(50) DEFAULT NULL,
  `overNightMethod` varchar(30) DEFAULT NULL,
  `isTrackingNumber` tinyint(1) DEFAULT NULL,
  `trackingNumber` varchar(50) DEFAULT NULL,
  `otherdeliverynotes` text DEFAULT NULL,
  `loanPackageType` varchar(100) DEFAULT NULL,
  `loanPackageTypeDesc` varchar(255) DEFAULT NULL,
  `lenderName` varchar(100) DEFAULT NULL,
  `customerFirstName` varchar(50) DEFAULT NULL,
  `customerLastName` varchar(50) DEFAULT NULL,
  `isSecondCustomer` tinyint(1) DEFAULT NULL,
  `secondCutomerName` varchar(100) DEFAULT NULL,
  `isThirdCustomer` tinyint(1) DEFAULT NULL,
  `thirdCustomerName` varchar(100) DEFAULT NULL,
  `isFourthCustomer` tinyint(1) DEFAULT NULL,
  `fourthCustomerName` varchar(100) DEFAULT NULL,
  `maritalStatus` varchar(20) DEFAULT NULL,
  `signingAddress` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `phone2` varchar(30) DEFAULT NULL,
  `phone3` varchar(30) DEFAULT NULL,
  `customerEmail` varchar(50) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `isFundDue` tinyint(1) DEFAULT NULL,
  `fundDueMethod` varchar(50) DEFAULT NULL,
  `fundsDue` varchar(20) DEFAULT NULL,
  `fundDuePaymentMethod1` varchar(20) DEFAULT NULL,
  `fundDuePaymentMethod2` varchar(20) DEFAULT NULL,
  `fundDuePaymentMethod3` varchar(20) DEFAULT NULL,
  `isSigningTrust` tinyint(1) DEFAULT NULL,
  `isTrusteeVerbiage` tinyint(1) DEFAULT NULL,
  `trusteeNotes` text DEFAULT NULL,
  `isPOA` tinyint(1) DEFAULT NULL,
  `isEsigning` tinyint(1) DEFAULT NULL,
  `esigningMethod` varchar(100) DEFAULT NULL,
  `isProvidentFunding` tinyint(1) DEFAULT NULL,
  `isBorrowerID` tinyint(1) DEFAULT NULL,
  `isInsuranceInformation` tinyint(1) DEFAULT NULL,
  `scanbacksRequired` tinyint(1) DEFAULT NULL,
  `isLoanOfficer` tinyint(1) DEFAULT NULL,
  `loanOfficerContact1` varchar(50) DEFAULT NULL,
  `loanOfficerContact2` varchar(50) DEFAULT NULL,
  `isCC` tinyint(1) DEFAULT NULL,
  `initialEmail` tinyint(1) DEFAULT NULL,
  `assignEmail` tinyint(1) DEFAULT NULL,
  `invoiceEmail` tinyint(1) DEFAULT NULL,
  `sameDayPickup` varchar(20) DEFAULT NULL,
  `assignDate` timestamp NULL DEFAULT NULL,
  `isDocsUploaded` tinyint(1) DEFAULT NULL,
  `sendDocsAnotherWay` varchar(255) DEFAULT NULL,
  `printingInstructions` varchar(200) DEFAULT NULL,
  `docsUploadedTime` timestamp NULL DEFAULT NULL,
  `isScanUploaded` tinyint(1) DEFAULT NULL,
  `scanUploadedTime` timestamp NULL DEFAULT NULL,
  `locatingSlection` varchar(50) DEFAULT NULL,
  `signingConfirmationToConsumer` tinyint(1) DEFAULT NULL,
  `globalFee` varchar(10) DEFAULT NULL,
  `visited` tinyint(1) DEFAULT NULL,
  `updateemailedtoclient` tinyint(1) DEFAULT NULL,
  `order_accepting_condition` text DEFAULT NULL,
  `unassigned_notaries` text DEFAULT NULL,
  `unassigned_reasons` text DEFAULT NULL,
  `admin_comments` text DEFAULT NULL,
  `search_notary_tab_opened` datetime DEFAULT NULL,
  `notary_pay` varchar(20) DEFAULT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `signingsrequests`
--

INSERT INTO `signingsrequests` (`orderID`, `clientID`, `notaryID`, `propertyAddress`, `typeofSigning`, `serviceType`, `borrowe1Name`, `borrower1Email`, `borrower1Cell`, `borrowe2Name`, `borrower2Email`, `borrower2Cell`, `orderNumber`, `status`, `fee`, `orderDate`, `clientFee`, `isScheduled`, `scheduleOption`, `scheduleTime`, `scheduleTimeCustom`, `scheduleOptionNotes`, `RequestedAppointmentDate`, `signingType`, `methodOfDelivery`, `EstimatedDocsAvailable`, `EstimatedPackageSize`, `overNightMethod`, `isTrackingNumber`, `trackingNumber`, `otherdeliverynotes`, `loanPackageType`, `loanPackageTypeDesc`, `lenderName`, `customerFirstName`, `customerLastName`, `isSecondCustomer`, `secondCutomerName`, `isThirdCustomer`, `thirdCustomerName`, `isFourthCustomer`, `fourthCustomerName`, `maritalStatus`, `signingAddress`, `city`, `zip`, `state`, `phone`, `phone2`, `phone3`, `customerEmail`, `comments`, `isFundDue`, `fundDueMethod`, `fundsDue`, `fundDuePaymentMethod1`, `fundDuePaymentMethod2`, `fundDuePaymentMethod3`, `isSigningTrust`, `isTrusteeVerbiage`, `trusteeNotes`, `isPOA`, `isEsigning`, `esigningMethod`, `isProvidentFunding`, `isBorrowerID`, `isInsuranceInformation`, `scanbacksRequired`, `isLoanOfficer`, `loanOfficerContact1`, `loanOfficerContact2`, `isCC`, `initialEmail`, `assignEmail`, `invoiceEmail`, `sameDayPickup`, `assignDate`, `isDocsUploaded`, `sendDocsAnotherWay`, `printingInstructions`, `docsUploadedTime`, `isScanUploaded`, `scanUploadedTime`, `locatingSlection`, `signingConfirmationToConsumer`, `globalFee`, `visited`, `updateemailedtoclient`, `order_accepting_condition`, `unassigned_notaries`, `unassigned_reasons`, `admin_comments`, `search_notary_tab_opened`, `notary_pay`, `userID`) VALUES
(6, NULL, 4, 'testing address', 'Purchase', 'Full Digitial RON E-closing', 'Muddassir', 'muddassar@mail.com', '2943857', 'Sajid', 'sajid@mail.com', '394857', '1231244', NULL, NULL, '2020-04-18 02:32:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `stateID` tinyint(3) UNSIGNED NOT NULL,
  `stateName` varchar(50) DEFAULT NULL,
  `orig_state_name` varchar(200) NOT NULL,
  `timezone_value` varchar(200) NOT NULL,
  `code` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`stateID`, `stateName`, `orig_state_name`, `timezone_value`, `code`) VALUES
(1, 'Alabama', 'ALABAMA', 'America/Chicago', 'AL'),
(2, 'Alaska', 'ALASKA', 'America/Anchorage', 'AK'),
(3, 'Arizona', 'ARIZONA', 'America/Phoenix', 'AZ'),
(4, 'Arkansas', 'ARKANSAS', 'America/Chicago', 'AR'),
(5, 'California', 'CALIFORNIA', 'America/Los_Angeles', 'CA'),
(6, 'Colorado', 'COLORADO', 'America/Denver', 'CO'),
(7, 'Connecticut', 'CONNECTICUT', 'America/New_York', 'CT'),
(8, 'Delaware', 'DELAWARE', 'America/New_York', 'DE'),
(9, 'Washington DC', 'COLUMBIA', 'America/New_York', 'DC'),
(10, 'Florida', 'FLORIDA', 'America/New_York', 'FL'),
(11, 'Georgia', 'GEORGIA', 'America/New_York', 'GA'),
(12, 'Hawaii', 'HAWAII', 'Pacific/Honolulu', 'HI'),
(13, 'Idaho', 'IDAHO', 'America/Denver', 'ID'),
(14, 'Illinois', 'ILLINOIS', 'America/Chicago', 'IL'),
(15, 'Indiana', '', 'America/New_York', 'IN'),
(16, 'Iowa', '', 'America/Chicago', 'IA'),
(17, 'Kansas', '', 'America/Chicago', 'KS'),
(18, 'Kentucky', '', 'America/New_York', 'KY'),
(19, 'Louisiana', '', 'America/Chicago', 'LA'),
(20, 'Maine', '', 'America/New_York', 'ME'),
(21, 'Maryland', '', 'America/New_York', 'MD'),
(22, 'Massachusetts', '', 'America/New_York', 'MA'),
(23, 'Michigan', '', 'America/New_York', 'MI'),
(24, 'Minnesota', '', 'America/Chicago', 'MN'),
(25, 'Mississippi', '', 'America/Chicago', 'MS'),
(26, 'Missouri', '', 'America/Chicago', 'MO'),
(27, 'Montana', '', 'America/Denver', 'MT'),
(28, 'Nebraska', '', 'America/Chicago', 'NE'),
(29, 'Nevada', '', 'America/Los_Angeles', 'NV'),
(30, 'New Hampshire', '', 'America/New_York', 'NH'),
(31, 'New Jersey', '', 'America/New_York', 'NJ'),
(32, 'New Mexico', '', 'America/Denver', 'NM'),
(33, 'New York', '', 'America/New_York', 'NY'),
(34, 'North Carolina', '', 'America/New_York', 'NC'),
(35, 'North Dakota', '', 'America/Chicago', 'ND'),
(36, 'Ohio', '', 'America/New_York', 'OH'),
(37, 'Oklahoma', '', 'America/Chicago', 'OK'),
(38, 'Oregon', '', 'America/Los_Angeles', 'OR'),
(39, 'Pennsylvania', '', 'America/New_York', 'PA'),
(40, 'Rhode Island', '', 'America/New_York', 'RI'),
(41, 'South Carolina', '', 'America/New_York', 'SC'),
(42, 'South Dakota', '', 'America/Chicago', 'SD'),
(43, 'Tennessee', '', 'America/Chicago', 'TN'),
(44, 'Texas', '', 'America/Chicago', 'TX'),
(45, 'Utah', '', 'America/Denver', 'UT'),
(46, 'Vermont', '', 'America/New_York', 'VT'),
(47, 'Virginia', '', 'America/New_York', 'VA'),
(48, 'Washington', '', 'America/Los_Angeles', 'WA'),
(49, 'West Virginia', '', 'America/New_York', 'WV'),
(50, 'Wisconsin', '', 'America/Chicago', 'WI'),
(51, 'Wyoming', '', 'America/Denver', 'WY');

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE `timezones` (
  `id` int(11) NOT NULL,
  `name` varchar(31) NOT NULL,
  `zone` varchar(272) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`id`, `name`, `zone`) VALUES
(1, '(UTC-11:00) Pacific/Pago_Pago', 'Pacific/Pago_Pago'),
(2, '(UTC-11:00) Pacific/Niue', 'Pacific/Niue'),
(3, '(UTC-11:00) Pacific/Midway', 'Pacific/Midway'),
(4, '(UTC-10:00) Pacific/Tahiti', 'Pacific/Tahiti'),
(5, '(UTC-10:00) America/Adak', 'America/Adak'),
(6, '(UTC-10:00) Pacific/Rarotonga', 'Pacific/Rarotonga'),
(7, '(UTC-10:00) Pacific/Honolulu', 'Pacific/Honolulu'),
(8, '(UTC-09:30) Pacific/Marquesas', 'Pacific/Marquesas'),
(9, '(UTC-09:00) Pacific/Gambier', 'Pacific/Gambier'),
(10, '(UTC-09:00) America/Sitka', 'America/Sitka'),
(11, '(UTC-09:00) America/Anchorage', 'America/Anchorage'),
(12, '(UTC-09:00) America/Yakutat', 'America/Yakutat'),
(13, '(UTC-09:00) America/Metlakatla', 'America/Metlakatla'),
(14, '(UTC-09:00) America/Juneau', 'America/Juneau'),
(15, '(UTC-09:00) America/Nome', 'America/Nome'),
(16, '(UTC-08:00) Pacific/Pitcairn', 'Pacific/Pitcairn'),
(17, '(UTC-08:00) America/Tijuana', 'America/Tijuana'),
(18, '(UTC-08:00) America/Vancouver', 'America/Vancouver'),
(19, '(UTC-08:00) America/Los_Angeles', 'America/Los_Angeles'),
(20, '(UTC-08:00) America/Whitehorse', 'America/Whitehorse'),
(21, '(UTC-08:00) America/Dawson', 'America/Dawson'),
(22, '(UTC-07:00) America/Cambridge_B', 'America/Cambridge_Bay'),
(23, '(UTC-07:00) America/Mazatlan', 'America/Mazatlan'),
(24, '(UTC-07:00) America/Boise', 'America/Boise'),
(25, '(UTC-07:00) America/Creston', 'America/Creston'),
(26, '(UTC-07:00) America/Yellowknife', 'America/Yellowknife'),
(27, '(UTC-07:00) America/Phoenix', 'America/Phoenix'),
(28, '(UTC-07:00) America/Chihuahua', 'America/Chihuahua'),
(29, '(UTC-07:00) America/Dawson_Cree', 'America/Dawson_Creek'),
(30, '(UTC-07:00) America/Inuvik', 'America/Inuvik'),
(31, '(UTC-07:00) America/Ojinaga', 'America/Ojinaga'),
(32, '(UTC-07:00) America/Denver', 'America/Denver'),
(33, '(UTC-07:00) America/Edmonton', 'America/Edmonton'),
(34, '(UTC-07:00) America/Hermosillo', 'America/Hermosillo'),
(35, '(UTC-07:00) America/Fort_Nelson', 'America/Fort_Nelson'),
(36, '(UTC-06:00) America/El_Salvador', 'America/El_Salvador'),
(37, '(UTC-06:00) America/Indiana/Tel', 'America/Indiana/Tell_City'),
(38, '(UTC-06:00) America/Costa_Rica', 'America/Costa_Rica'),
(39, '(UTC-06:00) America/Indiana/Kno', 'America/Indiana/Knox'),
(40, '(UTC-06:00) America/Bahia_Bande', 'America/Bahia_Banderas'),
(41, '(UTC-06:00) America/Guatemala', 'America/Guatemala'),
(42, '(UTC-06:00) America/Belize', 'America/Belize'),
(43, '(UTC-06:00) America/Managua', 'America/Managua'),
(44, '(UTC-06:00) America/Swift_Curre', 'America/Swift_Current'),
(45, '(UTC-06:00) America/Mexico_City', 'America/Mexico_City'),
(46, '(UTC-06:00) America/Resolute', 'America/Resolute'),
(47, '(UTC-06:00) America/Regina', 'America/Regina'),
(48, '(UTC-06:00) America/Rankin_Inle', 'America/Rankin_Inlet'),
(49, '(UTC-06:00) America/Rainy_River', 'America/Rainy_River'),
(50, '(UTC-06:00) America/North_Dakot', 'America/North_Dakota/New_Salem'),
(51, '(UTC-06:00) America/North_Dakot', 'America/North_Dakota/Center'),
(52, '(UTC-06:00) America/North_Dakot', 'America/North_Dakota/Beulah'),
(53, '(UTC-06:00) America/Tegucigalpa', 'America/Tegucigalpa'),
(54, '(UTC-06:00) America/Monterrey', 'America/Monterrey'),
(55, '(UTC-06:00) Pacific/Galapagos', 'Pacific/Galapagos'),
(56, '(UTC-06:00) America/Chicago', 'America/Chicago'),
(57, '(UTC-06:00) America/Merida', 'America/Merida'),
(58, '(UTC-06:00) America/Winnipeg', 'America/Winnipeg'),
(59, '(UTC-06:00) America/Menominee', 'America/Menominee'),
(60, '(UTC-06:00) America/Matamoros', 'America/Matamoros'),
(61, '(UTC-05:00) America/Iqaluit', 'America/Iqaluit'),
(62, '(UTC-05:00) America/Rio_Branco', 'America/Rio_Branco'),
(63, '(UTC-05:00) America/Lima', 'America/Lima'),
(64, '(UTC-05:00) America/Kentucky/Mo', 'America/Kentucky/Monticello'),
(65, '(UTC-05:00) America/Kentucky/Lo', 'America/Kentucky/Louisville'),
(66, '(UTC-05:00) America/Cayman', 'America/Cayman'),
(67, '(UTC-05:00) America/Pangnirtung', 'America/Pangnirtung'),
(68, '(UTC-05:00) America/Panama', 'America/Panama'),
(69, '(UTC-05:00) America/Jamaica', 'America/Jamaica'),
(70, '(UTC-05:00) America/Detroit', 'America/Detroit'),
(71, '(UTC-05:00) America/Indiana/Win', 'America/Indiana/Winamac'),
(72, '(UTC-05:00) America/Eirunepe', 'America/Eirunepe'),
(73, '(UTC-05:00) America/Indiana/Vin', 'America/Indiana/Vincennes'),
(74, '(UTC-05:00) America/New_York', 'America/New_York'),
(75, '(UTC-05:00) America/Grand_Turk', 'America/Grand_Turk'),
(76, '(UTC-05:00) America/Nassau', 'America/Nassau'),
(77, '(UTC-05:00) America/Guayaquil', 'America/Guayaquil'),
(78, '(UTC-05:00) America/Havana', 'America/Havana'),
(79, '(UTC-05:00) America/Indiana/Ind', 'America/Indiana/Indianapolis'),
(80, '(UTC-05:00) America/Indiana/Mar', 'America/Indiana/Marengo'),
(81, '(UTC-05:00) America/Indiana/Pet', 'America/Indiana/Petersburg'),
(82, '(UTC-05:00) America/Indiana/Vev', 'America/Indiana/Vevay'),
(83, '(UTC-05:00) America/Nipigon', 'America/Nipigon'),
(84, '(UTC-05:00) America/Port-au-Pri', 'America/Port-au-Prince'),
(85, '(UTC-05:00) America/Thunder_Bay', 'America/Thunder_Bay'),
(86, '(UTC-05:00) America/Cancun', 'America/Cancun'),
(87, '(UTC-05:00) America/Bogota', 'America/Bogota'),
(88, '(UTC-05:00) Pacific/Easter', 'Pacific/Easter'),
(89, '(UTC-05:00) America/Toronto', 'America/Toronto'),
(90, '(UTC-05:00) America/Atikokan', 'America/Atikokan'),
(91, '(UTC-04:00) America/Marigot', 'America/Marigot'),
(92, '(UTC-04:00) America/St_Barthele', 'America/St_Barthelemy'),
(93, '(UTC-04:00) America/St_Kitts', 'America/St_Kitts'),
(94, '(UTC-04:00) America/St_Lucia', 'America/St_Lucia'),
(95, '(UTC-04:00) America/La_Paz', 'America/La_Paz'),
(96, '(UTC-04:00) America/St_Thomas', 'America/St_Thomas'),
(97, '(UTC-04:00) America/St_Vincent', 'America/St_Vincent'),
(98, '(UTC-04:00) America/Lower_Princ', 'America/Lower_Princes'),
(99, '(UTC-04:00) America/Thule', 'America/Thule'),
(100, '(UTC-04:00) America/Manaus', 'America/Manaus'),
(101, '(UTC-04:00) America/Caracas', 'America/Caracas'),
(102, '(UTC-04:00) America/Martinique', 'America/Martinique'),
(103, '(UTC-04:00) America/Antigua', 'America/Antigua'),
(104, '(UTC-04:00) America/Tortola', 'America/Tortola'),
(105, '(UTC-04:00) America/Moncton', 'America/Moncton'),
(106, '(UTC-04:00) America/Montserrat', 'America/Montserrat'),
(107, '(UTC-04:00) Atlantic/Bermuda', 'Atlantic/Bermuda'),
(108, '(UTC-04:00) America/Santo_Domin', 'America/Santo_Domingo'),
(109, '(UTC-04:00) America/Port_of_Spa', 'America/Port_of_Spain'),
(110, '(UTC-04:00) America/Porto_Velho', 'America/Porto_Velho'),
(111, '(UTC-04:00) America/Puerto_Rico', 'America/Puerto_Rico'),
(112, '(UTC-04:00) America/Anguilla', 'America/Anguilla'),
(113, '(UTC-04:00) America/Kralendijk', 'America/Kralendijk'),
(114, '(UTC-04:00) America/Halifax', 'America/Halifax'),
(115, '(UTC-04:00) America/Curacao', 'America/Curacao'),
(116, '(UTC-04:00) America/Barbados', 'America/Barbados'),
(117, '(UTC-04:00) America/Glace_Bay', 'America/Glace_Bay'),
(118, '(UTC-04:00) America/Goose_Bay', 'America/Goose_Bay'),
(119, '(UTC-04:00) America/Grenada', 'America/Grenada'),
(120, '(UTC-04:00) America/Guadeloupe', 'America/Guadeloupe'),
(121, '(UTC-04:00) America/Dominica', 'America/Dominica'),
(122, '(UTC-04:00) America/Blanc-Sablo', 'America/Blanc-Sablon'),
(123, '(UTC-04:00) America/Aruba', 'America/Aruba'),
(124, '(UTC-04:00) America/Guyana', 'America/Guyana'),
(125, '(UTC-04:00) America/Boa_Vista', 'America/Boa_Vista'),
(126, '(UTC-03:30) America/St_Johns', 'America/St_Johns'),
(127, '(UTC-03:00) America/Paramaribo', 'America/Paramaribo'),
(128, '(UTC-03:00) Atlantic/Stanley', 'Atlantic/Stanley'),
(129, '(UTC-03:00) America/Cuiaba', 'America/Cuiaba'),
(130, '(UTC-03:00) America/Santiago', 'America/Santiago'),
(131, '(UTC-03:00) America/Belem', 'America/Belem'),
(132, '(UTC-03:00) America/Miquelon', 'America/Miquelon'),
(133, '(UTC-03:00) America/Campo_Grand', 'America/Campo_Grande'),
(134, '(UTC-03:00) America/Argentina/S', 'America/Argentina/Salta'),
(135, '(UTC-03:00) America/Punta_Arena', 'America/Punta_Arenas'),
(136, '(UTC-03:00) Antarctica/Palmer', 'Antarctica/Palmer'),
(137, '(UTC-03:00) America/Recife', 'America/Recife'),
(138, '(UTC-03:00) America/Bahia', 'America/Bahia'),
(139, '(UTC-03:00) America/Montevideo', 'America/Montevideo'),
(140, '(UTC-03:00) Antarctica/Rothera', 'Antarctica/Rothera'),
(141, '(UTC-03:00) America/Asuncion', 'America/Asuncion'),
(142, '(UTC-03:00) America/Argentina/S', 'America/Argentina/San_Juan'),
(143, '(UTC-03:00) America/Argentina/R', 'America/Argentina/Rio_Gallegos'),
(144, '(UTC-03:00) America/Argentina/M', 'America/Argentina/Mendoza'),
(145, '(UTC-03:00) America/Argentina/L', 'America/Argentina/La_Rioja'),
(146, '(UTC-03:00) America/Argentina/J', 'America/Argentina/Jujuy'),
(147, '(UTC-03:00) America/Argentina/C', 'America/Argentina/Cordoba'),
(148, '(UTC-03:00) America/Argentina/C', 'America/Argentina/Catamarca'),
(149, '(UTC-03:00) America/Argentina/B', 'America/Argentina/Buenos_Aires'),
(150, '(UTC-03:00) America/Araguaina', 'America/Araguaina'),
(151, '(UTC-03:00) America/Argentina/U', 'America/Argentina/Ushuaia'),
(152, '(UTC-03:00) America/Santarem', 'America/Santarem'),
(153, '(UTC-03:00) America/Cayenne', 'America/Cayenne'),
(154, '(UTC-03:00) America/Argentina/S', 'America/Argentina/San_Luis'),
(155, '(UTC-03:00) America/Fortaleza', 'America/Fortaleza'),
(156, '(UTC-03:00) America/Maceio', 'America/Maceio'),
(157, '(UTC-03:00) America/Godthab', 'America/Godthab'),
(158, '(UTC-03:00) America/Argentina/T', 'America/Argentina/Tucuman'),
(159, '(UTC-02:00) America/Noronha', 'America/Noronha'),
(160, '(UTC-02:00) America/Sao_Paulo', 'America/Sao_Paulo'),
(161, '(UTC-02:00) Atlantic/South_Geor', 'Atlantic/South_Georgia'),
(162, '(UTC-01:00) Atlantic/Azores', 'Atlantic/Azores'),
(163, '(UTC-01:00) Atlantic/Cape_Verde', 'Atlantic/Cape_Verde'),
(164, '(UTC-01:00) America/Scoresbysun', 'America/Scoresbysund'),
(165, '(UTC+00:00) Atlantic/St_Helena', 'Atlantic/St_Helena'),
(166, '(UTC+00:00) Africa/Accra', 'Africa/Accra'),
(167, '(UTC+00:00) Atlantic/Reykjavik', 'Atlantic/Reykjavik'),
(168, '(UTC+00:00) Antarctica/Troll', 'Antarctica/Troll'),
(169, '(UTC+00:00) Atlantic/Faroe', 'Atlantic/Faroe'),
(170, '(UTC+00:00) Europe/London', 'Europe/London'),
(171, '(UTC+00:00) Europe/Lisbon', 'Europe/Lisbon'),
(172, '(UTC+00:00) Atlantic/Canary', 'Atlantic/Canary'),
(173, '(UTC+00:00) Europe/Jersey', 'Europe/Jersey'),
(174, '(UTC+00:00) Europe/Isle_of_Man', 'Europe/Isle_of_Man'),
(175, '(UTC+00:00) Europe/Guernsey', 'Europe/Guernsey'),
(176, '(UTC+00:00) Atlantic/Madeira', 'Atlantic/Madeira'),
(177, '(UTC+00:00) Africa/Abidjan', 'Africa/Abidjan'),
(178, '(UTC+00:00) Europe/Dublin', 'Europe/Dublin'),
(179, '(UTC+00:00) Africa/Monrovia', 'Africa/Monrovia'),
(180, '(UTC+00:00) America/Danmarkshav', 'America/Danmarkshavn'),
(181, '(UTC+00:00) Africa/El_Aaiun', 'Africa/El_Aaiun'),
(182, '(UTC+00:00) Africa/Freetown', 'Africa/Freetown'),
(183, '(UTC+00:00) Africa/Dakar', 'Africa/Dakar'),
(184, '(UTC+00:00) Africa/Conakry', 'Africa/Conakry'),
(185, '(UTC+00:00) Africa/Bissau', 'Africa/Bissau'),
(186, '(UTC+00:00) Africa/Lome', 'Africa/Lome'),
(187, '(UTC+00:00) Africa/Banjul', 'Africa/Banjul'),
(188, '(UTC+00:00) Africa/Bamako', 'Africa/Bamako'),
(189, '(UTC+00:00) Africa/Casablanca', 'Africa/Casablanca'),
(190, '(UTC+00:00) Africa/Nouakchott', 'Africa/Nouakchott'),
(191, '(UTC+00:00) Africa/Ouagadougou', 'Africa/Ouagadougou'),
(192, '(UTC+00:00) Africa/Sao_Tome', 'Africa/Sao_Tome'),
(193, '(UTC+01:00) Europe/Rome', 'Europe/Rome'),
(194, '(UTC+01:00) Europe/Budapest', 'Europe/Budapest'),
(195, '(UTC+01:00) Europe/San_Marino', 'Europe/San_Marino'),
(196, '(UTC+01:00) Europe/Sarajevo', 'Europe/Sarajevo'),
(197, '(UTC+01:00) Europe/Skopje', 'Europe/Skopje'),
(198, '(UTC+01:00) Europe/Stockholm', 'Europe/Stockholm'),
(199, '(UTC+01:00) Europe/Belgrade', 'Europe/Belgrade'),
(200, '(UTC+01:00) Europe/Podgorica', 'Europe/Podgorica'),
(201, '(UTC+01:00) Europe/Tirane', 'Europe/Tirane'),
(202, '(UTC+01:00) Europe/Vaduz', 'Europe/Vaduz'),
(203, '(UTC+01:00) Europe/Vatican', 'Europe/Vatican'),
(204, '(UTC+01:00) Europe/Busingen', 'Europe/Busingen'),
(205, '(UTC+01:00) Europe/Vienna', 'Europe/Vienna'),
(206, '(UTC+01:00) Europe/Copenhagen', 'Europe/Copenhagen'),
(207, '(UTC+01:00) Europe/Warsaw', 'Europe/Warsaw'),
(208, '(UTC+01:00) Europe/Prague', 'Europe/Prague'),
(209, '(UTC+01:00) Europe/Monaco', 'Europe/Monaco'),
(210, '(UTC+01:00) Europe/Paris', 'Europe/Paris'),
(211, '(UTC+01:00) Europe/Bratislava', 'Europe/Bratislava'),
(212, '(UTC+01:00) Europe/Amsterdam', 'Europe/Amsterdam'),
(213, '(UTC+01:00) Africa/Algiers', 'Africa/Algiers'),
(214, '(UTC+01:00) Europe/Berlin', 'Europe/Berlin'),
(215, '(UTC+01:00) Europe/Ljubljana', 'Europe/Ljubljana'),
(216, '(UTC+01:00) Africa/Bangui', 'Africa/Bangui'),
(217, '(UTC+01:00) Europe/Luxembourg', 'Europe/Luxembourg'),
(218, '(UTC+01:00) Africa/Brazzaville', 'Africa/Brazzaville'),
(219, '(UTC+01:00) Europe/Oslo', 'Europe/Oslo'),
(220, '(UTC+01:00) Europe/Zurich', 'Europe/Zurich'),
(221, '(UTC+01:00) Africa/Ceuta', 'Africa/Ceuta'),
(222, '(UTC+01:00) Europe/Brussels', 'Europe/Brussels'),
(223, '(UTC+01:00) Europe/Madrid', 'Europe/Madrid'),
(224, '(UTC+01:00) Europe/Malta', 'Europe/Malta'),
(225, '(UTC+01:00) Europe/Andorra', 'Europe/Andorra'),
(226, '(UTC+01:00) Europe/Zagreb', 'Europe/Zagreb'),
(227, '(UTC+01:00) Europe/Gibraltar', 'Europe/Gibraltar'),
(228, '(UTC+01:00) Africa/Ndjamena', 'Africa/Ndjamena'),
(229, '(UTC+01:00) Africa/Libreville', 'Africa/Libreville'),
(230, '(UTC+01:00) Africa/Malabo', 'Africa/Malabo'),
(231, '(UTC+01:00) Africa/Tunis', 'Africa/Tunis'),
(232, '(UTC+01:00) Africa/Kinshasa', 'Africa/Kinshasa'),
(233, '(UTC+01:00) Africa/Luanda', 'Africa/Luanda'),
(234, '(UTC+01:00) Africa/Porto-Novo', 'Africa/Porto-Novo'),
(235, '(UTC+01:00) Africa/Niamey', 'Africa/Niamey'),
(236, '(UTC+01:00) Africa/Douala', 'Africa/Douala'),
(237, '(UTC+01:00) Africa/Lagos', 'Africa/Lagos'),
(238, '(UTC+02:00) Africa/Maputo', 'Africa/Maputo'),
(239, '(UTC+02:00) Asia/Nicosia', 'Asia/Nicosia'),
(240, '(UTC+02:00) Africa/Lusaka', 'Africa/Lusaka'),
(241, '(UTC+02:00) Europe/Tallinn', 'Europe/Tallinn'),
(242, '(UTC+02:00) Africa/Lubumbashi', 'Africa/Lubumbashi'),
(243, '(UTC+02:00) Europe/Sofia', 'Europe/Sofia'),
(244, '(UTC+02:00) Europe/Vilnius', 'Europe/Vilnius'),
(245, '(UTC+02:00) Africa/Blantyre', 'Africa/Blantyre'),
(246, '(UTC+02:00) Africa/Bujumbura', 'Africa/Bujumbura'),
(247, '(UTC+02:00) Africa/Cairo', 'Africa/Cairo'),
(248, '(UTC+02:00) Africa/Kigali', 'Africa/Kigali'),
(249, '(UTC+02:00) Africa/Khartoum', 'Africa/Khartoum'),
(250, '(UTC+02:00) Asia/Amman', 'Asia/Amman'),
(251, '(UTC+02:00) Europe/Riga', 'Europe/Riga'),
(252, '(UTC+02:00) Europe/Mariehamn', 'Europe/Mariehamn'),
(253, '(UTC+02:00) Africa/Gaborone', 'Africa/Gaborone'),
(254, '(UTC+02:00) Europe/Uzhgorod', 'Europe/Uzhgorod'),
(255, '(UTC+02:00) Europe/Kiev', 'Europe/Kiev'),
(256, '(UTC+02:00) Africa/Johannesburg', 'Africa/Johannesburg'),
(257, '(UTC+02:00) Asia/Jerusalem', 'Asia/Jerusalem'),
(258, '(UTC+02:00) Asia/Damascus', 'Asia/Damascus'),
(259, '(UTC+02:00) Africa/Windhoek', 'Africa/Windhoek'),
(260, '(UTC+02:00) Europe/Chisinau', 'Europe/Chisinau'),
(261, '(UTC+02:00) Africa/Tripoli', 'Africa/Tripoli'),
(262, '(UTC+02:00) Asia/Famagusta', 'Asia/Famagusta'),
(263, '(UTC+02:00) Asia/Gaza', 'Asia/Gaza'),
(264, '(UTC+02:00) Asia/Hebron', 'Asia/Hebron'),
(265, '(UTC+02:00) Europe/Bucharest', 'Europe/Bucharest'),
(266, '(UTC+02:00) Europe/Athens', 'Europe/Athens'),
(267, '(UTC+02:00) Africa/Harare', 'Africa/Harare'),
(268, '(UTC+02:00) Europe/Zaporozhye', 'Europe/Zaporozhye'),
(269, '(UTC+02:00) Africa/Mbabane', 'Africa/Mbabane'),
(270, '(UTC+02:00) Europe/Helsinki', 'Europe/Helsinki'),
(271, '(UTC+02:00) Africa/Maseru', 'Africa/Maseru'),
(272, '(UTC+02:00) Asia/Beirut', 'Asia/Beirut'),
(273, '(UTC+02:00) Europe/Kaliningrad', 'Europe/Kaliningrad'),
(274, '(UTC+03:00) Africa/Mogadishu', 'Africa/Mogadishu'),
(275, '(UTC+03:00) Europe/Kirov', 'Europe/Kirov'),
(276, '(UTC+03:00) Africa/Addis_Ababa', 'Africa/Addis_Ababa'),
(277, '(UTC+03:00) Africa/Kampala', 'Africa/Kampala'),
(278, '(UTC+03:00) Europe/Istanbul', 'Europe/Istanbul'),
(279, '(UTC+03:00) Africa/Asmara', 'Africa/Asmara'),
(280, '(UTC+03:00) Africa/Juba', 'Africa/Juba'),
(281, '(UTC+03:00) Europe/Minsk', 'Europe/Minsk'),
(282, '(UTC+03:00) Antarctica/Syowa', 'Antarctica/Syowa'),
(283, '(UTC+03:00) Africa/Nairobi', 'Africa/Nairobi'),
(284, '(UTC+03:00) Indian/Mayotte', 'Indian/Mayotte'),
(285, '(UTC+03:00) Europe/Moscow', 'Europe/Moscow'),
(286, '(UTC+03:00) Asia/Riyadh', 'Asia/Riyadh'),
(287, '(UTC+03:00) Indian/Comoro', 'Indian/Comoro'),
(288, '(UTC+03:00) Indian/Antananarivo', 'Indian/Antananarivo'),
(289, '(UTC+03:00) Africa/Dar_es_Salaa', 'Africa/Dar_es_Salaam'),
(290, '(UTC+03:00) Africa/Djibouti', 'Africa/Djibouti'),
(291, '(UTC+03:00) Europe/Volgograd', 'Europe/Volgograd'),
(292, '(UTC+03:00) Asia/Kuwait', 'Asia/Kuwait'),
(293, '(UTC+03:00) Asia/Aden', 'Asia/Aden'),
(294, '(UTC+03:00) Asia/Baghdad', 'Asia/Baghdad'),
(295, '(UTC+03:00) Asia/Qatar', 'Asia/Qatar'),
(296, '(UTC+03:00) Europe/Simferopol', 'Europe/Simferopol'),
(297, '(UTC+03:00) Asia/Bahrain', 'Asia/Bahrain'),
(298, '(UTC+03:30) Asia/Tehran', 'Asia/Tehran'),
(299, '(UTC+04:00) Europe/Saratov', 'Europe/Saratov'),
(300, '(UTC+04:00) Asia/Baku', 'Asia/Baku'),
(301, '(UTC+04:00) Indian/Reunion', 'Indian/Reunion'),
(302, '(UTC+04:00) Asia/Tbilisi', 'Asia/Tbilisi'),
(303, '(UTC+04:00) Europe/Samara', 'Europe/Samara'),
(304, '(UTC+04:00) Asia/Yerevan', 'Asia/Yerevan'),
(305, '(UTC+04:00) Asia/Muscat', 'Asia/Muscat'),
(306, '(UTC+04:00) Europe/Ulyanovsk', 'Europe/Ulyanovsk'),
(307, '(UTC+04:00) Indian/Mahe', 'Indian/Mahe'),
(308, '(UTC+04:00) Asia/Dubai', 'Asia/Dubai'),
(309, '(UTC+04:00) Indian/Mauritius', 'Indian/Mauritius'),
(310, '(UTC+04:00) Europe/Astrakhan', 'Europe/Astrakhan'),
(311, '(UTC+04:30) Asia/Kabul', 'Asia/Kabul'),
(312, '(UTC+05:00) Indian/Kerguelen', 'Indian/Kerguelen'),
(313, '(UTC+05:00) Asia/Dushanbe', 'Asia/Dushanbe'),
(314, '(UTC+05:00) Indian/Maldives', 'Indian/Maldives'),
(315, '(UTC+05:00) Asia/Tashkent', 'Asia/Tashkent'),
(316, '(UTC+05:00) Asia/Karachi', 'Asia/Karachi'),
(317, '(UTC+05:00) Asia/Samarkand', 'Asia/Samarkand'),
(318, '(UTC+05:00) Asia/Yekaterinburg', 'Asia/Yekaterinburg'),
(319, '(UTC+05:00) Asia/Aqtau', 'Asia/Aqtau'),
(320, '(UTC+05:00) Antarctica/Mawson', 'Antarctica/Mawson'),
(321, '(UTC+05:00) Asia/Oral', 'Asia/Oral'),
(322, '(UTC+05:00) Asia/Atyrau', 'Asia/Atyrau'),
(323, '(UTC+05:00) Asia/Ashgabat', 'Asia/Ashgabat'),
(324, '(UTC+05:00) Asia/Aqtobe', 'Asia/Aqtobe'),
(325, '(UTC+05:30) Asia/Kolkata', 'Asia/Kolkata'),
(326, '(UTC+05:30) Asia/Colombo', 'Asia/Colombo'),
(327, '(UTC+05:45) Asia/Kathmandu', 'Asia/Kathmandu'),
(328, '(UTC+06:00) Indian/Chagos', 'Indian/Chagos'),
(329, '(UTC+06:00) Asia/Almaty', 'Asia/Almaty'),
(330, '(UTC+06:00) Asia/Urumqi', 'Asia/Urumqi'),
(331, '(UTC+06:00) Asia/Bishkek', 'Asia/Bishkek'),
(332, '(UTC+06:00) Asia/Qyzylorda', 'Asia/Qyzylorda'),
(333, '(UTC+06:00) Antarctica/Vostok', 'Antarctica/Vostok'),
(334, '(UTC+06:00) Asia/Dhaka', 'Asia/Dhaka'),
(335, '(UTC+06:00) Asia/Omsk', 'Asia/Omsk'),
(336, '(UTC+06:00) Asia/Thimphu', 'Asia/Thimphu'),
(337, '(UTC+06:30) Indian/Cocos', 'Indian/Cocos'),
(338, '(UTC+06:30) Asia/Yangon', 'Asia/Yangon'),
(339, '(UTC+07:00) Asia/Pontianak', 'Asia/Pontianak'),
(340, '(UTC+07:00) Asia/Phnom_Penh', 'Asia/Phnom_Penh'),
(341, '(UTC+07:00) Indian/Christmas', 'Indian/Christmas'),
(342, '(UTC+07:00) Asia/Novokuznetsk', 'Asia/Novokuznetsk'),
(343, '(UTC+07:00) Asia/Jakarta', 'Asia/Jakarta'),
(344, '(UTC+07:00) Asia/Hovd', 'Asia/Hovd'),
(345, '(UTC+07:00) Asia/Ho_Chi_Minh', 'Asia/Ho_Chi_Minh'),
(346, '(UTC+07:00) Asia/Bangkok', 'Asia/Bangkok'),
(347, '(UTC+07:00) Asia/Krasnoyarsk', 'Asia/Krasnoyarsk'),
(348, '(UTC+07:00) Asia/Novosibirsk', 'Asia/Novosibirsk'),
(349, '(UTC+07:00) Asia/Tomsk', 'Asia/Tomsk'),
(350, '(UTC+07:00) Asia/Vientiane', 'Asia/Vientiane'),
(351, '(UTC+07:00) Antarctica/Davis', 'Antarctica/Davis'),
(352, '(UTC+07:00) Asia/Barnaul', 'Asia/Barnaul'),
(353, '(UTC+08:00) Asia/Irkutsk', 'Asia/Irkutsk'),
(354, '(UTC+08:00) Asia/Hong_Kong', 'Asia/Hong_Kong'),
(355, '(UTC+08:00) Asia/Kuala_Lumpur', 'Asia/Kuala_Lumpur'),
(356, '(UTC+08:00) Asia/Kuching', 'Asia/Kuching'),
(357, '(UTC+08:00) Asia/Macau', 'Asia/Macau'),
(358, '(UTC+08:00) Australia/Perth', 'Australia/Perth'),
(359, '(UTC+08:00) Asia/Makassar', 'Asia/Makassar'),
(360, '(UTC+08:00) Asia/Manila', 'Asia/Manila'),
(361, '(UTC+08:00) Asia/Ulaanbaatar', 'Asia/Ulaanbaatar'),
(362, '(UTC+08:00) Asia/Singapore', 'Asia/Singapore'),
(363, '(UTC+08:00) Asia/Taipei', 'Asia/Taipei'),
(364, '(UTC+08:00) Asia/Choibalsan', 'Asia/Choibalsan'),
(365, '(UTC+08:00) Asia/Brunei', 'Asia/Brunei'),
(366, '(UTC+08:00) Asia/Shanghai', 'Asia/Shanghai'),
(367, '(UTC+08:30) Asia/Pyongyang', 'Asia/Pyongyang'),
(368, '(UTC+08:45) Australia/Eucla', 'Australia/Eucla'),
(369, '(UTC+09:00) Asia/Dili', 'Asia/Dili'),
(370, '(UTC+09:00) Asia/Chita', 'Asia/Chita'),
(371, '(UTC+09:00) Asia/Khandyga', 'Asia/Khandyga'),
(372, '(UTC+09:00) Asia/Jayapura', 'Asia/Jayapura'),
(373, '(UTC+09:00) Asia/Seoul', 'Asia/Seoul'),
(374, '(UTC+09:00) Pacific/Palau', 'Pacific/Palau'),
(375, '(UTC+09:00) Asia/Tokyo', 'Asia/Tokyo'),
(376, '(UTC+09:00) Asia/Yakutsk', 'Asia/Yakutsk'),
(377, '(UTC+09:30) Australia/Darwin', 'Australia/Darwin'),
(378, '(UTC+10:00) Asia/Ust-Nera', 'Asia/Ust-Nera'),
(379, '(UTC+10:00) Pacific/Saipan', 'Pacific/Saipan'),
(380, '(UTC+10:00) Pacific/Guam', 'Pacific/Guam'),
(381, '(UTC+10:00) Antarctica/DumontDU', 'Antarctica/DumontDUrville'),
(382, '(UTC+10:00) Asia/Vladivostok', 'Asia/Vladivostok'),
(383, '(UTC+10:00) Australia/Lindeman', 'Australia/Lindeman'),
(384, '(UTC+10:00) Australia/Brisbane', 'Australia/Brisbane'),
(385, '(UTC+10:00) Pacific/Port_Moresb', 'Pacific/Port_Moresby'),
(386, '(UTC+10:00) Pacific/Chuuk', 'Pacific/Chuuk'),
(387, '(UTC+10:30) Australia/Adelaide', 'Australia/Adelaide'),
(388, '(UTC+10:30) Australia/Broken_Hi', 'Australia/Broken_Hill'),
(389, '(UTC+11:00) Pacific/Guadalcanal', 'Pacific/Guadalcanal'),
(390, '(UTC+11:00) Antarctica/Casey', 'Antarctica/Casey'),
(391, '(UTC+11:00) Antarctica/Macquari', 'Antarctica/Macquarie'),
(392, '(UTC+11:00) Pacific/Kosrae', 'Pacific/Kosrae'),
(393, '(UTC+11:00) Pacific/Norfolk', 'Pacific/Norfolk'),
(394, '(UTC+11:00) Pacific/Noumea', 'Pacific/Noumea'),
(395, '(UTC+11:00) Pacific/Pohnpei', 'Pacific/Pohnpei'),
(396, '(UTC+11:00) Australia/Sydney', 'Australia/Sydney'),
(397, '(UTC+11:00) Pacific/Efate', 'Pacific/Efate'),
(398, '(UTC+11:00) Australia/Melbourne', 'Australia/Melbourne'),
(399, '(UTC+11:00) Australia/Lord_Howe', 'Australia/Lord_Howe'),
(400, '(UTC+11:00) Australia/Hobart', 'Australia/Hobart'),
(401, '(UTC+11:00) Australia/Currie', 'Australia/Currie'),
(402, '(UTC+11:00) Asia/Srednekolymsk', 'Asia/Srednekolymsk'),
(403, '(UTC+11:00) Pacific/Bougainvill', 'Pacific/Bougainville'),
(404, '(UTC+11:00) Asia/Sakhalin', 'Asia/Sakhalin'),
(405, '(UTC+11:00) Asia/Magadan', 'Asia/Magadan'),
(406, '(UTC+12:00) Pacific/Funafuti', 'Pacific/Funafuti'),
(407, '(UTC+12:00) Asia/Kamchatka', 'Asia/Kamchatka'),
(408, '(UTC+12:00) Pacific/Wake', 'Pacific/Wake'),
(409, '(UTC+12:00) Pacific/Tarawa', 'Pacific/Tarawa'),
(410, '(UTC+12:00) Pacific/Wallis', 'Pacific/Wallis'),
(411, '(UTC+12:00) Pacific/Fiji', 'Pacific/Fiji'),
(412, '(UTC+12:00) Pacific/Nauru', 'Pacific/Nauru'),
(413, '(UTC+12:00) Asia/Anadyr', 'Asia/Anadyr'),
(414, '(UTC+12:00) Pacific/Majuro', 'Pacific/Majuro'),
(415, '(UTC+12:00) Pacific/Kwajalein', 'Pacific/Kwajalein'),
(416, '(UTC+13:00) Antarctica/McMurdo', 'Antarctica/McMurdo'),
(417, '(UTC+13:00) Pacific/Enderbury', 'Pacific/Enderbury'),
(418, '(UTC+13:00) Pacific/Tongatapu', 'Pacific/Tongatapu'),
(419, '(UTC+13:00) Pacific/Fakaofo', 'Pacific/Fakaofo'),
(420, '(UTC+13:00) Pacific/Auckland', 'Pacific/Auckland'),
(421, '(UTC+13:45) Pacific/Chatham', 'Pacific/Chatham'),
(422, '(UTC+14:00) Pacific/Apia', 'Pacific/Apia'),
(423, '(UTC+14:00) Pacific/Kiritimati', 'Pacific/Kiritimati');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(16) NOT NULL,
  `lname` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `avatar` varchar(256) DEFAULT NULL,
  `status` enum('Active','Suspended','Inactive') NOT NULL,
  `token` varchar(256) DEFAULT NULL,
  `company` int(11) NOT NULL,
  `role` enum('admin','staff','superadmin','user','notary','client') NOT NULL DEFAULT 'user',
  `permissions` varchar(256) NOT NULL DEFAULT '["upload","edit","delete"]',
  `signature` varchar(500) DEFAULT NULL,
  `lastnotification` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `hiddenfiles` text DEFAULT NULL,
  `timezone` varchar(256) NOT NULL DEFAULT 'Africa/Nairobi',
  `lang` varchar(16) NOT NULL DEFAULT 'en_US',
  `city` varchar(100) NOT NULL,
  `state` int(11) NOT NULL,
  `zip` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `phone`, `address`, `password`, `avatar`, `status`, `token`, `company`, `role`, `permissions`, `signature`, `lastnotification`, `hiddenfiles`, `timezone`, `lang`, `city`, `state`, `zip`) VALUES
(1, 'Daniel', 'Kimeli', 'demo@simcycreative.com', '720783834', NULL, 'b69532e166822c98b737fd003e4895b69e3b8a8ccef7301301c88b167abcf73b', NULL, 'Active', NULL, 1, 'superadmin', '[\"upload\",\"edit\",\"delete\"]', NULL, '2019-06-15 16:49:33', NULL, 'Africa/Nairobi', 'en_US', '', 0, ''),
(17, 'test', 'test', 'test@simcycreative.com', '1213213213', 'test', '68e411debb5f8aedb1df2e8860796c4e021ce842b689373deac972404ad5e435', NULL, 'Active', NULL, 0, 'notary', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-16 20:00:55', NULL, 'America/New_York', 'en_US', '', 0, ''),
(18, 'test', 'client', 'ccccc@simcycreative.com', '4385767', 'mailing address', '68e411debb5f8aedb1df2e8860796c4e021ce842b689373deac972404ad5e435', NULL, 'Active', NULL, 0, 'notary', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-16 21:00:35', NULL, 'America/Chicago', 'en_US', '', 0, ''),
(19, 'Adam', 'Babish', 'lockflower@gmail.com', '4086560007', '401 Chestnut Apar', '3998813b7163e80093e80c5e3291add5e0e7a7d51ac3753b18390007dbb31790', NULL, 'Active', NULL, 0, 'notary', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-16 21:17:04', NULL, 'America/Los_Angeles', 'en_US', '', 0, ''),
(20, 'Adam', 'Babish', 'ccc@zigsig.com', '418', '401 Chestn', '004724b6815c2d74eb50bf28437f47fdf2db51d626c3df5f887ed4c38ad6520f', NULL, 'Active', NULL, 0, 'notary', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-16 21:25:14', NULL, 'America/Los_Angeles', 'en_US', '', 0, ''),
(25, 'asd', 'asd', 'lockflowesr@gmail.com', 'asd', 'asd', 'eaf66fd94b54099dbe04efc0ebac7a52dac0a0e543cb118634fc6dbdf3fe468d', NULL, 'Active', NULL, 0, 'notary', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-17 17:53:36', NULL, 'America/Chicago', 'en_US', 'asd', 16, '234'),
(28, 'asd', 'asd', 'lockflowers@gmail.com', 'asd', 'sddd', 'eaf66fd94b54099dbe04efc0ebac7a52dac0a0e543cb118634fc6dbdf3fe468d', NULL, 'Active', NULL, 0, 'notary', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-17 18:05:36', NULL, 'America/Chicago', 'en_US', 'aaaa', 16, 'asd'),
(30, 'asd', 'asd', 'lockflowesr@gmail.com', 'asd', 'asd', 'eaf66fd94b54099dbe04efc0ebac7a52dac0a0e543cb118634fc6dbdf3fe468d', NULL, 'Active', NULL, 0, 'notary', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-17 18:09:01', NULL, 'America/Chicago', 'en_US', 'asd', 16, 'sss'),
(31, 'asd', 'asd', 'asd@MAIL.COM', 'asd', 'asd', 'd7e80cdf47a933ee942c153922c0fc527b46bc785a438a066d568caa3d898ae6', 'chUMDijdLg9V6GI4INH209n6QlWgsjo6.png', 'Active', NULL, 1, 'user', '[\"upload\",\"edit\",\"delete\"]', NULL, '2020-04-17 21:18:39', NULL, 'Africa/Nairobi', 'en_US', '', 0, '');

-- --------------------------------------------------------

--
-- Structure for view `clientusers`
--
DROP TABLE IF EXISTS `clientusers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `clientusers`  AS  select `c`.`clientID` AS `clientID`,`c`.`company` AS `company`,`c`.`businessPhone` AS `businessPhone`,`c`.`afterHoursPhone` AS `afterHoursPhone`,`c`.`birthDay` AS `birthDay`,`c`.`birthMonth` AS `birthMonth`,`c`.`defaultPayment` AS `defaultPayment`,`c`.`printFee` AS `printFee`,`c`.`noSignFee` AS `noSignFee`,`c`.`redrawFee` AS `redrawFee`,`c`.`cancelledFee` AS `cancelledFee`,`c`.`addtionalTerms` AS `addtionalTerms`,`c`.`shippingMethod` AS `shippingMethod`,`c`.`isShippingAccountNumber` AS `isShippingAccountNumber`,`c`.`shippingAccountNumber` AS `shippingAccountNumber`,`c`.`shippingAccountLabel` AS `shippingAccountLabel`,`c`.`orderNotificationEmails` AS `orderNotificationEmails`,`c`.`serviceType` AS `serviceType`,`c`.`customTierFee` AS `customTierFee`,`c`.`emailDocsPrice` AS `emailDocsPrice`,`c`.`sellerSigningsPrice` AS `sellerSigningsPrice`,`c`.`firstAndSecondPrice` AS `firstAndSecondPrice`,`c`.`invoiceTo` AS `invoiceTo`,`c`.`firstVisitonDashboard` AS `firstvisitondashboard`,`c`.`firstVisitonSearch` AS `firstvisitonsearch`,`c`.`weeklyInvoice` AS `weeklyInvoice`,`c`.`emailPlan` AS `emailPlan`,`c`.`noAttachAssignOrder` AS `noAttachAssignOrder`,`c`.`blockProfile` AS `blockProfile`,`c`.`userID` AS `userID` from (`clientdetails` `c` join `users` `u`) where `c`.`userID` = `u`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `notaryusers`
--
DROP TABLE IF EXISTS `notaryusers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `notaryusers`  AS  select `n`.`notaryID` AS `notaryID`,`n`.`phone` AS `phone`,`n`.`phone2` AS `phone2`,`n`.`phone3` AS `phone3`,`n`.`video` AS `video`,`n`.`introduction` AS `introduction`,`n`.`canReceiveDocViaEmail` AS `canReceiveDocViaEmail`,`n`.`haveLaserPrinter` AS `haveLaserPrinter`,`n`.`canPrintLegalSize` AS `canPrintLegalSize`,`n`.`haveADualTrayPrinter` AS `haveADualTrayPrinter`,`n`.`foreignLanguages` AS `foreignLanguages`,`n`.`notaryExperience` AS `notaryExperience`,`n`.`haveExperienceInSigning` AS `haveExperienceInSigning`,`n`.`canContactReference` AS `canContactReference`,`n`.`signingsPerformed` AS `signingsPerformed`,`n`.`loanSigningMethod` AS `loanSigningMethod`,`n`.`notaryComissionNumber` AS `notaryComissionNumber`,`n`.`notaryComissionExpiration` AS `notaryComissionExpiration`,`n`.`isBounder` AS `isBounder`,`n`.`boundNumber` AS `boundNumber`,`n`.`boundAmount` AS `boundAmount`,`n`.`boundExpiration` AS `boundExpiration`,`n`.`isInsurance` AS `isInsurance`,`n`.`insuranceNumber` AS `insuranceNumber`,`n`.`insuranceAmount` AS `insuranceAmount`,`n`.`insuranceExpiration` AS `insuranceExpiration`,`n`.`fideltyApproved` AS `fideltyApproved`,`n`.`fideltyApprovalType` AS `fideltyApprovalType`,`n`.`additionalInformation` AS `additionalInformation`,`n`.`prolinkSignings` AS `prolinkSignings`,`n`.`textReminder` AS `textReminder`,`n`.`preferredContactMethodSMS` AS `preferredContactMethodSMS`,`n`.`preferredContactMethodCall` AS `preferredContactMethodCall`,`n`.`color` AS `color`,`n`.`notaryScore` AS `notaryScore`,`n`.`omit` AS `omit`,`n`.`stripeConnectedAccount` AS `stripeConnectedAccount`,`n`.`stripeAccountVerified` AS `stripeAccountVerified`,`n`.`signature` AS `signature`,`n`.`slug` AS `slug`,`n`.`adminFav` AS `adminFav`,`n`.`eliteUser` AS `eliteUser`,`n`.`facebookID` AS `facebookID`,`n`.`instagramID` AS `instagramID`,`n`.`website` AS `website`,`n`.`car` AS `car`,`n`.`taxID` AS `taxID`,`n`.`ssnOrEin` AS `ssnOrEin`,`n`.`photoApprove` AS `photoApprove`,`n`.`visitCount` AS `visitCount`,`n`.`lastNotaryLogin` AS `lastnotarylogin`,`n`.`smsDisabled` AS `smsDisabled`,`n`.`hideNotary` AS `hideNotary`,`n`.`oneTimeAccess` AS `oneTimeAccess`,`n`.`userID` AS `userID` from (`notarydetails` `n` join `users` `u`) where `n`.`userID` = `u`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender` (`sender`),
  ADD KEY `sender_2` (`sender`),
  ADD KEY `file` (`file`);

--
-- Indexes for table `clientdetails`
--
ALTER TABLE `clientdetails`
  ADD PRIMARY KEY (`clientID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departmentmembers`
--
ALTER TABLE `departmentmembers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department` (`department`),
  ADD KEY `member` (`member`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company` (`company`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company` (`company`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `sharing_key` (`document_key`),
  ADD KEY `company` (`company`),
  ADD KEY `uploaded_by` (`uploaded_by`),
  ADD KEY `folder` (`folder`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `company` (`company`),
  ADD KEY `folder` (`folder`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `company` (`company`),
  ADD KEY `file` (`file`),
  ADD KEY `file_2` (`file`);

--
-- Indexes for table `notarydetails`
--
ALTER TABLE `notarydetails`
  ADD PRIMARY KEY (`notaryID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company` (`company`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file` (`document`),
  ADD KEY `company` (`company`),
  ADD KEY `user` (`sender`);

--
-- Indexes for table `signingsrequests`
--
ALTER TABLE `signingsrequests`
  ADD PRIMARY KEY (`orderID`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`stateID`),
  ADD UNIQUE KEY `stateID` (`stateID`);

--
-- Indexes for table `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company` (`company`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clientdetails`
--
ALTER TABLE `clientdetails`
  MODIFY `clientID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departmentmembers`
--
ALTER TABLE `departmentmembers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notarydetails`
--
ALTER TABLE `notarydetails`
  MODIFY `notaryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `signingsrequests`
--
ALTER TABLE `signingsrequests`
  MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `stateID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `timezones`
--
ALTER TABLE `timezones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=424;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`file`) REFERENCES `files` (`document_key`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`sender`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clientdetails`
--
ALTER TABLE `clientdetails`
  ADD CONSTRAINT `clientDetails_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Constraints for table `departmentmembers`
--
ALTER TABLE `departmentmembers`
  ADD CONSTRAINT `departmentmembers_ibfk_1` FOREIGN KEY (`department`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `departmentmembers_ibfk_2` FOREIGN KEY (`member`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fields_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fields_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`uploaded_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `files_ibfk_2` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `files_ibfk_3` FOREIGN KEY (`folder`) REFERENCES `folders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `folders`
--
ALTER TABLE `folders`
  ADD CONSTRAINT `folders_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `folders_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `folders_ibfk_3` FOREIGN KEY (`folder`) REFERENCES `folders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`file`) REFERENCES `files` (`document_key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notarydetails`
--
ALTER TABLE `notarydetails`
  ADD CONSTRAINT `notaryDetails_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reminders`
--
ALTER TABLE `reminders`
  ADD CONSTRAINT `reminders_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `requests_ibfk_2` FOREIGN KEY (`document`) REFERENCES `files` (`document_key`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `requests_ibfk_3` FOREIGN KEY (`sender`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
